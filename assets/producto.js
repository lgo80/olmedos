import './styles/producto.css';

const bobina = [true, false, false, false, false, false, false, true, false];
const parlante = [false, false, true, true, false, false, false, false, false];
const reparacion = [false, true, true, true, true, true, true, true, false];
$("#Producto_tipoProducto").change(function () {
    const valorSeleccionado = $("#Producto_tipoProducto").val();
    switch (parseInt(valorSeleccionado)) {
        case 1:
            estadoDeLosTextos(bobina);
            break;
        case 2:
            estadoDeLosTextos(parlante);
            break;
        case 3:
            estadoDeLosTextos(reparacion);
            break;
    }

});

function estadoDeLosTextos(habilitar) {
    $("#Producto_detalle").prop("disabled", habilitar[0]);
    $("#Producto_diametro").prop("disabled", habilitar[1]);
    $("#Producto_bobinado").prop("disabled", habilitar[2]);
    $("#Producto_altura").prop("disabled", habilitar[3]);
    $("#Producto_impedancia").prop("disabled", habilitar[4]);
    $("#Producto_tipoBobina").prop("disabled", habilitar[5]);
    $("#Producto_material").prop("disabled", habilitar[6]);
    $("#Producto_potencia").prop("disabled", habilitar[7]);
    $("#Producto_nota").prop("disabled", habilitar[8]);
}