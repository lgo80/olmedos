import './styles/app.css';

$("#btnSeleccionarTodo").click(function () {
    const casilleros = document.getElementsByClassName('casillero');
    const seleccionarTodo = $('#btnSeleccionarTodo');
    for (let i = 0; i < casilleros.length; i++) {
        casilleros[i].checked = (seleccionarTodo.html().trim() === 'Seleccionar todo');
    }
    const texto = (seleccionarTodo.html().trim() === 'Seleccionar todo') ? 'Deseleccionar todo' : 'Seleccionar todo';
    seleccionarTodo.html(texto);
});

$(".cmbVerPago").click(function () {
    const sujeto = $(this).data('sujeto');
    const periodo = $(this).data('periodo');
    $("#cmbPersona").val(sujeto);
    $("#txtPeriodo").val(periodo);
    $("#cmbEnviar").click();
});