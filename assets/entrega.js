import './styles/entrega.css';

$(document).on('change', '.classDetallePedidoEntrega', function () {
    const idDetalle = $(this).val();
    const selectId = $(this).attr('id');
    const postData = {idDetalle: idDetalle};
    const match = selectId.match(/Entrega_detalleEntregas_(\d+)_detallePedido/);
    if (match) {
        procesarPrecioYResultados(match[1], postData);
    }
});

$(document).on('click', '.classPrecioEntrega', function () {
    const selectId = $(this).attr('id');
    const match = selectId.match(/Entrega_detalleEntregas_(\d+)_precio/);
    const idDetalle = $('#Entrega_detalleEntregas_' + match[1] + '_detallePedido').val();
    const postData = {idDetalle: idDetalle};
    if (match) {
        procesarPrecioYResultados(match[1], postData);
    }
});

function procesarPrecioYResultados(indice, postData) {
    $.ajax({
        url: "/admin/devolverprecioycantidad/",
        method: "POST",
        data: postData, // Datos POST
        success: function (data) {
            $('#Entrega_detalleEntregas_' + indice + '_precio').val(data.precio);
            $('#Entrega_detalleEntregas_' + indice + '_cantidad').val(data.cantidad);
        },
        error: function () {
            console.error("Error al obtener los datos");
        }
    });
}