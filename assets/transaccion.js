import './styles/transaccion.css';

$(document).on('change', '#Transaccion_tipo', function () {
    const valor = $(this).val();
    if (valor === '1') {
        $('#Transaccion_monto_base').val(1);
        $('.baseImporte .items-placeholder').val('A favor de nosotros');
    } else if (valor === '2') {
        $('#Transaccion_monto_base').val(-1);
        $('.baseImporte .items-placeholder').val('A favor del cliente');
    } else if (valor === '3') {
        $('#Transaccion_monto_base').val(-1);
        $('.baseImporte .items-placeholder').val('A favor del cliente');
    }
});
