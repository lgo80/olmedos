import './styles/caja.css';

$("#aplicarFiltro").click(function () {
    const fechadesde = $('#fechadesde').val();
    const fechahasta = $('#fechahasta').val();
    if (fechadesde === "" || fechahasta === "") {
        $('#txtErrorFechasModal').text('Las fechas desde y hasta no pueden estar vacias');
        $('#btnPrueba').click();
    } else if (fechadesde >= fechahasta) {
        $('#txtErrorFechasModal').text('La fecha desde tiene que ser anterior a la fecha hasta');
        $('#btnPrueba').click();
    } else {
        $('#aplicarFiltrosubmit').click();
    }
});

$("#btnOrdenAscCaja").click(function () {
    if (!$(this).hasClass('text-info')) {
        aplicarOrden('asc');
    }
});

$("#btnOrdenDescCaja").click(function () {
    if (!$(this).hasClass('text-info')) {
        aplicarOrden('desc');
    }
});

function aplicarOrden(valor) {
    $('#txtOrdenCaja').val(valor);
    $('#aplicarFiltrosubmit').click();
}