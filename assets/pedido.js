import './styles/pedido.css';

$(document).on('change', '.classProductoPedido', function () {
    const selectId = $(this).attr('id');
    const postData = {idProducto: $(this).val(), idPersona: $("#Pedido_persona").val()};
    const match = selectId.match(/Pedido_detallePedidos_(\d+)_producto/);
    if (match) {
        procesarPrecio(match[1], postData)
    }
});

function procesarPrecio(indice, postData) {
    $.ajax({
        url: "/admin/devolverprecio/",
        method: "POST",
        data: postData, // Datos POST
        success: function (data) {
            $('#Pedido_detallePedidos_' + indice + '_precio').val(data.precio);
        },
        error: function (error) {
            console.error(error);
        }
    });
}

$(document).on('click', '.classPrecioPedido', function () {
    const selectId = $(this).attr('id');
    const match = selectId.match(/Pedido_detallePedidos_(\d+)_precio/);
    const idDetalle = $('#Pedido_detallePedidos_' + match[1] + '_producto').val();
    const postData = {idProducto: idDetalle, idPersona: $("#Pedido_persona").val()};
    if (match) {
        procesarPrecio(match[1], postData);
    }
});