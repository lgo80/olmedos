SHELL := /bin/bash

tests:
	php bin/console doctrine:database:drop --force || true
	php bin/console doctrine:database:create
	php bin/console doctrine:schema:create
	php bin/console doctrine:fixtures:load -n
.PHONY: tests