const visibilidad = {
    "mostrar": "block",
    "ocultar": "none",
    "inline": "inline"
};

function validar() {
    let respuesta = true;
    let mensaje = "";
    let i;

    function emailCheck(emailStr) {
        const emailPat = /^(.+)@(.+)$/;
        const specialChars = "\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
        const validChars = "\[^\\s" + specialChars + "\]";
        const quotedUser = "(\"[^\"]*\")";
        const ipDomainPat = /^[(d{1,3}).(d{1,3}).(d{1,3}).(d{1,3})]$/;
        const atom = validChars + '+';
        const word = "(" + atom + "|" + quotedUser + ")";
        const userPat = new RegExp("^" + word + "(\\." + word + ")*$");
        const domainPat = new RegExp("^" + atom + "(\\." + atom + ")*$");
        const matchArray = emailStr.match(emailPat);
        if (matchArray == null) {
            mensaje += "La dirección de correo parece ser inválida (verifique las @ y .)\n";
            return false;
        }
        const user = matchArray[1]
        const domain = matchArray[2]
        if (user.match(userPat) == null) {
            mensaje += "El nombre de usuario parece ser inválido\n";
            return false;
        }

        const IPArray = domain.match(ipDomainPat)
        if (IPArray != null) {
            for (let i = 1; i <= 4; i++) {
                if (IPArray[i] > 255) {
                    mensaje += "La dirección IP de destino es inválida\n";
                    return false;
                }
            }
            return true;
        }

        const domainArray = domain.match(domainPat);
        if (domainArray == null) {
            mensaje += "El dominio no parece ser válido\n";
            return false;
        }
        const atomPat = new RegExp(atom, "g");
        const domArr = domain.match(atomPat);
        const len = domArr.length;
        if (domArr[domArr.length - 1].length < 2 ||
            domArr[domArr.length - 1].length > 3) {
            mensaje += "Las direcciones deben terminar con dominios de tres letras, o el código de país de dos letras\n";
            return false;
        }

        if (len < 2) {
            mensaje += "Dominio Inválido\n";
            return false;
        }
        return true;
    }

    mensaje = "Los siguientes campos tienen que ser completados:\n";
    for (i = 1; i <= 4; i++) {
        if (document.getElementById(i).value === "") {
            respuesta = false;
            mensaje += document.getElementById(i).name + "\n";
        } else {
            if ((i === 2) && (!emailCheck(document.getElementById(i).value)))
                respuesta = false;
        }
    }
    if (respuesta === false) {
        alert(mensaje);
    }
    return respuesta;

}

function MM_swapImgRestore() { //v3.0
    let i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
        x.src = x.oSrc;
}

function MM_preloadImages() { //v3.0
    const d = document;
    if (d.images) {
        if (!d.MM_p)
            d.MM_p = [];
        let i, j = d.MM_p.length, a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") !== 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}

function MM_findObj(n, d) { //v4.01
    let p, i, x;
    if (!d)
        d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all)
        x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++)
        x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++)
        x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById)
        x = d.getElementById(n);
    return x;
}

function MM_swapImage() { //v3.0
    let i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = [];
    for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc)
                x.oSrc = x.src;
            x.src = a[i + 2];
        }
}

function cerrarMostrarErrores(id) {
    if ($(id).css("display") === visibilidad.ocultar) {
        $(id).css("display", visibilidad.mostrar);
    } else {
        $(id).css("display", visibilidad.ocultar);
        $('#limpiar').focus().click();
    }
}

function mandarComentario() {
    const resultado = validar();
    if (resultado) {
        aplicarAjaxEnvioMail();
    }
}

function aplicarAjaxEnvioMail() {
    const nombre = document.getElementById("1").value;
    const mail = document.getElementById("2").value;
    const telefono = document.getElementById("5").value;
    const asunto = document.getElementById("3").value;
    const comentario = document.getElementById("4").value;
    $.ajax({
        type: "POST",
        url: "js/mandar2.php",
        data: "nombre=" + nombre + "&mail=" + mail + "&telefono=" + telefono + "&asunto=" + asunto + "&comentario=" + comentario,
        success: function (datos) {
            const obj = JSON.parse(datos);
            if (!obj.result) {
                document.getElementById("problemas").innerHTML = "¡Hubo problemas de conexion a internet!<br />vuelva a interntar mas tarde por favor";
            }
            cerrarMostrarErrores('#apDiv1');
        }
    });
}