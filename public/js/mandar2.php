<?php
$mensaje = array(
    "result" => FALSE
);
$miEmail = "ventas.olym@gmail.com";
if (!$_POST) {
    exit;
}
if (!defined("PHP_EOL")){
    define("PHP_EOL", "\r\n");
}
$name = $_POST['nombre'];
$email = $_POST['mail'];
$phone = $_POST['telefono'];
$subject = $_POST['asunto'];
$comments = $_POST['comentario'];
$address = $miEmail;
$e_subject = $subject; //'Nuevo mensaje de ' . $name . '.'; 
$e_body = "El señor $name hizo una consulta con respecto a $subject y la pregunta es:" . PHP_EOL . PHP_EOL;
$e_content = "$comments" . PHP_EOL . PHP_EOL;
$e_reply = "Podras contactar a $name via email, $email";
if ($phone != "")
    $e_reply .= " o via telefono $phone";
$msg = wordwrap($e_body . $e_content . $e_reply, 70);
$headers = "From: Pagina Olmedos <$miEmail> " . PHP_EOL; //$email
$headers .= "Reply-To: $email" . PHP_EOL;
$headers .= "MIME-Version: 1.0" . PHP_EOL;
$headers .= "Content-type: text/plain; charset=utf-8" . PHP_EOL;
$headers .= "Content-Transfer-Encoding: quoted-printable" . PHP_EOL;
if (mail($address, $e_subject, $msg, $headers)) {
    $mensaje["result"] = TRUE;
} 
//Convierte la variable $mensaje a formato Json
$json_result = json_encode($mensaje);
//le avisa a PHP que el formato que se envia es tipo Json
header('conten-type: application/json');
//Devuelve la variable tipo Json que se creo 
echo $json_result;
?>