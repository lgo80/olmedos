<?php

namespace App\Form;

use App\Entity\Caja;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CajaFechaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            /*->add('fechadesde', DateTimeType::class, [
                'placeholder' => 'Seleccionar fecha de inicio',
                'widget' => 'single_text', // Usa un solo campo de texto para la fecha
                'html5' => false, // Desactiva la entrada HTML5 para permitir personalización
                'format' => 'yyyy-MM-dd\THH:mm',
                'mapped' => false
            ])
            ->add('fechahasta', DateTimeType::class, [
                'placeholder' => 'Seleccionar fecha de fin',
                'widget' => 'single_text', // Usa un solo campo de texto para la fecha
                'html5' => false, // Desactiva la entrada HTML5 para permitir personalización
                'format' => 'yyyy-MM-dd\THH:mm',
                'mapped' => false
            ])*/
            ->add('fechadesde', TextType::class, [
                'mapped' => false,
                'label' => 'Ingresar fecha desde',
                'help' => 'El formato debe ser día/mes/año',
                'required' => true,
            ])
            ->add('fechahasta', TextType::class, [
                'mapped' => false,
                'label' => 'Ingresar fecha hasta',
                'help' => 'El formato debe ser día/mes/año',
                'required' => true,
            ])
            ->add('guardar', SubmitType::class, ['label' => 'Filtrar']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Caja::class,
        ]);
    }
}
