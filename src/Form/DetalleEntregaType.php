<?php

namespace App\Form;

use App\Entity\DetalleEntrega;
use App\Entity\DetallePedido;
use App\Repository\DetallePedidoRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DetalleEntregaType extends AbstractType
{
    /*public function __construct(
        private readonly DetallePedidoRepository $detallePedidoRepository
    )
    {
    }*/

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('detallePedido', EntityType::class, [
                'class' => DetallePedido::class,
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('dp')
                        ->where('dp.username', 'ASC');
                },
                'choice_label' => 'Detalle de pedido',
            ])
            ->add('cantidad')
            ->add('precio');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => DetalleEntrega::class,
        ]);
    }
}
