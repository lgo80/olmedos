<?php

namespace App\Form;

use App\Entity\ListaDePrecios;
use App\Entity\ListaPreciosPersona;
use App\Entity\Persona;
use App\Repository\PersonaRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListaPreciosPersonaType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('persona', EntityType::class, [
                // looks for choices from this entity
                'class' => Persona::class,
                'query_builder' => function (EntityRepository $er): QueryBuilder {
                    return $er->createQueryBuilder('p')
                        ->where('p.tipo != 2')
                        ->orderBy('p.razonSocial', 'ASC')
                        ->orderBy('p.nombre', 'ASC');
                },
                // uses the User.username property as the visible option string
//                'choice_label' => 'Persona',

                // used to render a select box, check boxes or radios
                // 'multiple' => true,
                // 'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ListaPreciosPersona::class,
        ]);
    }
}
