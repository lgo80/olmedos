<?php

namespace App\Form;

use App\Entity\Moneda;
use App\Entity\Monto;
use App\Entity\Venta;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\PositiveOrZero;

class VentaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fechaVenta', DateTimeType::class, [
                'html5' => true,
                'widget' => 'single_text'
            ])
            ->add('importe', NumberType::class, [
                'html5' => true,
                'mapped' => false,
                'constraints' => [
                    new PositiveOrZero([
                        'message' => 'Ingrese un numero positivo mayor o igual a cero',
                    ]),
                ]
                /*'choice_label' => function (Monto $monto) {
                    // Define cómo se muestra cada opción en el formulario
                    return $monto->getImporte() . ' ' . $monto->getMoneda();
                },*/
//                'placeholder' => 'Selecciona un monto', // Opcional: un texto para mostrar como opción inicial
                // Otros parámetros si es necesario
            ])
            ->add('moneda', EntityType::class, [
                'required' => true,
                'class' => Moneda::class,
                'mapped' => false
            ]);
        /*->add('monto', EntityType::class, [
            'required' => true,
            'constraints' => [
                new PositiveOrZero([
                    'message' => 'Ingrese un numero positivo mayor o igual a cero',
                ]),
            ],
        ]);*/
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Venta::class,
        ]);
    }
}
