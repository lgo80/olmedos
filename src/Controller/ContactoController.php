<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ContactoController extends AbstractController
{
    #[Route('/contacto', name: 'app_contacto')]
    public function index(): Response
    {
        return $this->render('contacto/index.html.twig');
    }

    #[Route('/contacto/enviar', name: 'app_contacto_enviar', methods: ['POST'])]
    public function enviarCorreo(Request $request, MailerInterface $mailer): Response
    {
        $nombre = $request->request->get('nombre');
        $emailUsuario = $request->request->get('mail');
        $telefono = $request->request->get('telefono');
        $asunto = $request->request->get('asunto');
        $comentario = $request->request->get('comentario');

        $email = (new Email())
            ->from($emailUsuario)
            ->to('ventas.olym@gmail.com')
            ->subject($asunto)
            ->html("
                <p><strong>Nombre:</strong> $nombre</p>
                <p><strong>Correo:</strong> $emailUsuario</p>
                <p><strong>Teléfono:</strong> $telefono</p>
                <p><strong>Mensaje:</strong></p>
                <p>$comentario</p>
            ");

        $mailer->send($email);

        $this->addFlash('success', 'Su mensaje ha sido enviado correctamente.');

        return $this->redirectToRoute('app_contacto');
    }
}
