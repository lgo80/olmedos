<?php

namespace App\Controller;

use App\Repository\DetallePedidoRepository;
use App\Repository\EntregaRepository;
use App\Repository\ListaDePreciosRepository;
use App\Repository\PedidoRepository;
use App\Repository\PersonaRepository;
use App\Repository\ProductoRepository;
use App\Repository\TransaccionRepository;
use App\Repository\UserRepository;
use App\Repository\VentaRepository;
use App\Service\Utils\PrincipalService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        return $this->render('home/index.html.twig');
    }

    #[Route('/prueba', name: 'app_prueba')]
    public function prueba(
        MailerInterface          $mailer,
        UserRepository           $userRepository,
        PersonaRepository        $personaRepository,
        PedidoRepository         $pedidoRepository,
        ProductoRepository       $productoRepository,
        TransaccionRepository    $transaccionRepository,
        DetallePedidoRepository  $detallePedidoRepository,
        VentaRepository          $ventaRepository,
        ListaDePreciosRepository $listaDePreciosRepository,
        PrincipalService         $principalService,
        EntregaRepository        $entregaRepository): Response
    {
        return new Response('Hola');
    }

}
