<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReparacionController extends AbstractController
{
    #[Route('/reparacion', name: 'app_reparacion')]
    public function index(): Response
    {
        return $this->render('reparacion/index.html.twig');
    }
}
