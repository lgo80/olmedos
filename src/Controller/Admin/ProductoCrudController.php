<?php

namespace App\Controller\Admin;

use App\Entity\Producto;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProductoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Producto::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')
                ->hideOnForm(),
            ChoiceField::new('tipoProducto', 'Tipo de producto')
                ->setChoices([
                    'Bobina' => 1,
                    'Parlante' => 2,
                    'Reparación' => 3
                ]),
            IdField::new('detalle', 'Detalle')
                ->onlyOnForms(),
            TextField::new('nombreCompleto', 'Nombre completo')
                ->onlyOnIndex(),
            NumberField::new('diametro', 'Diametro interno')
                ->hideOnIndex(),
            IntegerField::new('bobinado', 'Altura de bobinado')
                ->hideOnIndex(),
            IntegerField::new('altura', 'Altura de la bobina')
                ->hideOnIndex(),
            IntegerField::new('impedancia', 'Impedancia'),
            ChoiceField::new('tipoBobina', 'Tipo de bobina')
                ->setChoices([
                    'Cinta' => 1,
                    'Alambre' => 2
                ])->onlyOnForms(),
            TextField::new('tipoBobinaStr', 'Tipo de bobina')
                ->onlyOnDetail(),
            ChoiceField::new('material', 'Material del bobinado')
                ->setChoices([
                    'Cobre' => 1,
                    'Aluminio' => 2
                ])->onlyOnForms(),
            TextField::new('materialStr', 'Material del bobinado')
                ->onlyOnDetail(),
            IntegerField::new('potencia', 'Potencia'),
            TextareaField::new('nota', 'Observacones')
                ->hideOnIndex(),
            AssociationField::new('clasificacionDeBobinas', 'Clasificación'),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL,
                fn(Action $action) => $action->setIcon('fa fa-eye')->setLabel('Ver'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->update(Crud::PAGE_DETAIL, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->update(Crud::PAGE_DETAIL, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_DETAIL, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'));

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Productos')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar producto')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear producto')
            ->showEntityActionsInlined()
            ->setDefaultSort(['detalle' => 'ASC', 'diametro' => 'ASC', 'bobinado' => 'ASC', 'impedancia' => 'ASC', 'material' => 'ASC', 'potencia' => 'ASC']);
    }
}
