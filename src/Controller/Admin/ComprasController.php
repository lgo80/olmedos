<?php

namespace App\Controller\Admin;

use App\Entity\Compra;
use App\Service\Utils\PrincipalService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ComprasController extends AbstractController
{
    #[Route('/admin/compras/{id}', name: 'app_compras')]
    public function show(
        Compra           $compra,
        PrincipalService $principalService): Response
    {
        return $this->render('admin/compras/show.html.twig', [
            'compra' => $compra,
            'principalService' => $principalService
        ]);
    }
}