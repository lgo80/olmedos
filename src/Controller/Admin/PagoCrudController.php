<?php

namespace App\Controller\Admin;

use App\Entity\Pago;
use App\Service\Utils\PrincipalService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PagoCrudController extends AbstractCrudController
{

    public function __construct(
        private readonly PrincipalService $principalService
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Pago::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')
                ->hideOnForm(),
            DateTimeField::new('fechaPago', 'Fecha del pago'),
            ChoiceField::new('tipo', 'Tipo de Pago')->setChoices([
                'Sueldo' => Pago::TIPO_SUELDO,
                'Impuesto' => Pago::TIPO_IMPUESTO,
                'Otros gastos' => Pago::TIPO_OTROS_GASTOS
            ]),
            ChoiceField::new('sujeto', 'Sujeto')->setChoices([
                'Leo' => Pago::SUJETO_LEO,
                'Mariano' => Pago::SUJETO_MARIANO,
                'Casa' => Pago::SUJETO_CASA,
                'Taller' => Pago::SUJETO_TALLER,
            ]),
            DateTimeField::new('periodo', 'Periodo')
                ->onlyOnForms(),
            TextareaField::new('periodoStr', 'Periodo')
                ->hideOnForm(),
            TextareaField::new('nota', 'Observaciones'),
            AssociationField::new('monto', 'Importe')
                ->renderAsEmbeddedForm()
                ->setCrudController(MontoCrudController::class)
                ->onlyOnForms(),
            TextField::new('montoFormateado', 'Importe')
                ->formatValue(function ($monto, Pago $entity) {
                    return $this->principalService->formatearMonto($entity->getMonto());
                })
                ->addCssClass('text-danger')
                ->onlyOnIndex(),
            TextField::new('monedaQueSePago', 'Moneda usada')
                ->onlyOnDetail(),
            TextField::new('importeQueSePago', 'Importe pagado')
                ->formatValue(function ($monto, Pago $entity) {
                    return $this->principalService->formatearNumero(
                        floatval($monto), false, $entity->getMonto()->getMoneda()->getSigno());
                })
                ->onlyOnDetail(),
            TextField::new('tipoCambioQueSePago', 'Tipo de cambio usado')
                ->formatValue(function ($monto) {
                    return $this->principalService->formatearNumero(floatval($monto), false, '$');
                })
                ->onlyOnDetail(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('sujeto')
            ->add('tipo')
            ->add('fechaPago');
    }

    /**
     */
    public function configureActions(Actions $actions): Actions
    {
        $goToStripe = Action::new('app_pago_ver_sueldo', 'Ver Sueldos', 'fa fa-line-chart')
            ->linkToRoute('app_pago_ver_sueldo')
            ->createAsGlobalAction();
        return $actions
            ->add(Crud::PAGE_INDEX, $goToStripe)
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL,
                fn(Action $action) => $action->setIcon('fa fa-eye')->setLabel('Ver'))
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->update(Crud::PAGE_DETAIL, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'));
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Pagos')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar pago')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear pago')
            ->showEntityActionsInlined()
            ->setDefaultSort(['fechaPago' => 'DESC']);
    }
}
