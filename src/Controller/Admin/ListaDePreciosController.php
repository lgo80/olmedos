<?php

namespace App\Controller\Admin;

use App\Entity\ListaDePrecios;
use App\Repository\ListaDePreciosRepository;
use App\Service\Utils\ListaDePreciosService;
use App\Service\Utils\PrincipalService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListaDePreciosController extends AbstractController
{

    public function __construct(
        private readonly AdminUrlGenerator $adminUrlGenerator)
    {
    }

    #[Route('/admin/listadeprecios/{id}', name: 'app_show_lista_de_precios')]
    public function show(
        ListaDePrecios   $listaDePrecios,
        PrincipalService $principalService): Response
    {
        return $this->render('admin/lista_de_precios/show.html.twig', [
            'listaDePrecios' => $listaDePrecios,
            'principalService' => $principalService,
        ]);
    }

    #[Route('/admin/listadeprecios/aumentar/precios', name: 'app_aumentar_lista_de_precios')]
    public function aumentar(
        ListaDePreciosRepository $listaDePreciosRepository,
        PrincipalService         $principalService
    ): Response
    {
        $listasDePrecios = $listaDePreciosRepository->findAll();
        return $this->render('admin/lista_de_precios/aumentar.html.twig', [
            'principalService' => $principalService,
            'listasDePrecios' => $listasDePrecios,
        ]);
    }

    #[Route('/admin/listadeprecios/aumentar/precios/store', name: 'app_aumentar_lista_de_precios_store', methods: ['POST'])]
    public function aumentarStore(
        ListaDePreciosService $listaDePreciosService,
        Request               $request,
    ): Response
    {
        /* $porcentaje = $request->request->get('txtPorcentaje');
         if ($porcentaje > 0)*/
        [$cajas, $error] = ($listaDePreciosService)($request);
        $url = ($error)
            ? $this->adminUrlGenerator->setRoute(
                'app_aumentar_lista_de_precios')->generateUrl()
            : $this->adminUrlGenerator
                ->setController(ListaDePreciosCrudController::class)
                ->setAction(Action::INDEX)
                ->generateUrl();
        return $this->redirect($url);
    }
}
