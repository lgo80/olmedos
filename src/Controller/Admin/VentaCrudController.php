<?php

namespace App\Controller\Admin;

use App\Entity\Venta;
use App\Service\Utils\PrincipalService;
use App\Service\Utils\VentaService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;

class VentaCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly PrincipalService $principalService,
        private readonly VentaService     $ventaService
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Venta::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')
                ->hideOnForm(),
            TextField::new('cliente', 'Cliente')
                ->hideOnForm(),
            DateTimeField::new('fechaVenta', 'Fecha de la venta')
                ->hideOnForm(),
            TextField::new('importeEntrega', 'Importe de la entrega')
                ->formatValue(function ($monto, Venta $venta) {
                    return $this->principalService->formatearNumero(
                        $this->ventaService->getImporteDeLaEntrega($venta), false,
                        $this->getUser()->getMoneda()->getSigno());
                })
                ->hideOnForm(),
            TextField::new('importePagado', 'Importe pagado')
                ->formatValue(function ($monto, Venta $venta) {
                    return $this->principalService->formatearMonto($venta->getMonto());
                })
                ->hideOnForm(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $sendInvoice = Action::new('app_venta_show', 'Ver', 'fa fa-eye')
            ->linkToRoute('app_venta_show', function (Venta $venta): array {
                return [
                    'id' => $venta->getId(),
                ];
            });
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::DELETE)
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->add(Crud::PAGE_INDEX, $sendInvoice);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Ventas')
            ->setPageTitle(Crud::PAGE_DETAIL, 'Ver venta')
            ->showEntityActionsInlined()
            ->setDefaultSort(['fechaVenta' => 'DESC']);
    }
}
