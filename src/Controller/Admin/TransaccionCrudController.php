<?php

namespace App\Controller\Admin;

use App\Entity\Transaccion;
use App\Service\Utils\PrincipalService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class TransaccionCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly PrincipalService $principalService
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Transaccion::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')
                ->hideOnForm(),
            DateTimeField::new('fechaTransaccion', 'Fecha de la transacción'),
            AssociationField::new('persona', 'Persona'),
            ChoiceField::new('tipo', 'Detalle')
                ->setChoices([
                    'Deuda' => Transaccion::TIPO_DEUDA,
                    'Anticipo' => Transaccion::TIPO_ANTICIPO,
                    'Descuento' => Transaccion::TIPO_DESCUENTO
                ])
                ->onlyOnForms(),
            TextField::new('tipoStr', 'Detalle')
                ->hideOnForm(),
            AssociationField::new('monto', 'Importe')
                ->renderAsEmbeddedForm()
                ->setCrudController(MontoCrudController::class)
                ->onlyOnForms(),
            TextField::new('montoEnStr', 'Importe')
                ->formatValue(function ($monto, Transaccion $entity) {
                    return $this->principalService->formatearMonto($entity->getMonto());
                })
                ->onlyOnIndex(),
            TextField::new('monedaQueSePago', 'Moneda usada')
                ->onlyOnDetail(),
            TextField::new('importeQueSePago', 'Importe pagado')
                ->formatValue(function ($monto, Transaccion $entity) {
                    return $this->principalService->formatearNumero(
                        floatval($monto), false, $entity->getMonto()->getMoneda()->getSigno());
                })
                ->onlyOnDetail(),
            TextField::new('tipoCambioQueSePago', 'Tipo de cambio usado')
                ->formatValue(function ($monto) {
                    return $this->principalService->formatearNumero(
                        floatval($monto), false, '$');
                })
                ->onlyOnDetail(),
            BooleanField::new('estaCobrado', 'Se cobro?')
                ->renderAsSwitch(false)
                ->hideWhenCreating(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('persona')
            ->add('tipo')
            ->add('estaCobrado');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL,
                fn(Action $action) => $action->setIcon('fa fa-eye')->setLabel('Ver'))
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->update(Crud::PAGE_DETAIL, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'));
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Transacciones')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar transacción')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear transacción')
            ->showEntityActionsInlined()
            ->setDefaultSort(['estaCobrado' => 'ASC']);
    }
}
