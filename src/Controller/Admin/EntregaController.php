<?php

namespace App\Controller\Admin;

use App\Entity\Entrega;
use App\Entity\Venta;
use App\Form\EntregaType;
use App\Form\VentaType;
use App\Repository\DetallePedidoRepository;
use App\Repository\EntregaRepository;
use App\Repository\PersonaRepository;
use App\Repository\TransaccionRepository;
use App\Service\Utils\EntregaService;
use App\Service\Utils\PrincipalService;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EntregaController extends AbstractController
{

    public function __construct(
        private readonly AdminUrlGenerator $adminUrlGenerator)
    {
    }

    #[Route('/admin/entrega/show/{id}', name: 'app_entrega_show')]
    public function show(
        Request               $request,
        PersonaRepository     $personaRepository,
        TransaccionRepository $transaccionRepository,
        Entrega               $entrega,
        PrincipalService      $principalService
    ): Response
    {
        if (!$entrega->isSePago())
            $transacciones = $transaccionRepository->findBy([
                'persona' => $entrega->getPersona(),
                'estaCobrado' => false
            ]);
        return $this->render('admin/entrega/show.html.twig', [
            'entrega' => $entrega,
            'transacciones' => $transacciones ?? null,
            'principalService' => $principalService
        ]);
    }

    #[Route('/admin/entrega/entregar/{id}', name: 'app_entrega_entregar')]
    public function entregar(
        Request           $request,
        PersonaRepository $personaRepository,
        EntregaRepository $entregaRepository,
        EntregaService    $entregaService,
        PrincipalService  $principalService,
        Entrega           $entrega
    ): Response
    {
        if ($entrega->isSeEntrego())
            return $this->redirectToRoute(
                'app_entrega_show',
                ['id' => $entrega],
                Response::HTTP_SEE_OTHER);
        $form = $this->createForm(EntregaType::class, $entrega);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entrega->setSeEntrego(true);
            $entregaRepository->save($entrega, true);
            $entregaService->actualizarPedidosEntregados($entrega);
            $url = $this->adminUrlGenerator->setRoute('app_entrega_show', [
                'id' => $entrega->getId(),
            ])->generateUrl();
            return $this->redirect($url);
        }
        return $this->render('admin/entrega/entregar.html.twig', [
            'entrega' => $entrega,
            'form' => $form->createView(),
            'service' => $principalService
        ]);
    }

    #[Route('/admin/entrega/cobrar/{id}', name: 'app_entrega_cobrar')]
    public function cobrar(
        Request               $request,
        PrincipalService      $principalService,
        TransaccionRepository $transaccionRepository,
        Entrega               $entrega,
        EntregaService        $entregaService
    ): Response
    {
        if ($entrega->isSePago()) {
            $url = $this->adminUrlGenerator->setRoute('app_entrega_show', [
                'id' => $entrega->getId(),
            ])->generateUrl();
            return $this->redirect($url);
        }
        $transacciones = $transaccionRepository->findBy([
            'persona' => $entrega->getPersona(),
            'estaCobrado' => false
        ]);
        $ventaNueva = new Venta();
        $ventaNueva->addEntrega($entrega);
        $form = $this->createForm(VentaType::class, $ventaNueva);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entregaService->guardarCobro(
                $entrega, $transacciones, $ventaNueva, $form);
            $entregaService->generarDeudaNueva(
                $ventaNueva, $this->getUser());
            $url = $this->adminUrlGenerator->setRoute(
                'app_entrega_show',
                [
                    'id' => $entrega->getId(),
                ])->generateUrl();
            return $this->redirect($url);
        }
        return $this->render('admin/entrega/cobrar.html.twig', [
            'entrega' => $entrega,
            'transacciones' => $transacciones,
            'form' => $form,
            'service' => $principalService
        ]);
    }

    #[Route('/admin/devolverprecioycantidad/', name: 'app_entrega_devolver_precio_cantidad')]
    public function devolverPrecio(
        Request                 $request,
        DetallePedidoRepository $detallePedidoRepository,
    ): JsonResponse
    {
        $idDetalle = $request->request->get('idDetalle');
        $detalle = $detallePedidoRepository->find($idDetalle);
        return new JsonResponse(['precio' => $detalle->getPrecio(), 'cantidad' => $detalle->getCantidad()]);
    }
}
