<?php

namespace App\Controller\Admin;

use App\Entity\Entrega;
use App\Entity\Persona;
use App\Service\Utils\PrincipalService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class EntregaCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly PrincipalService $principalService
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Entrega::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')
                ->hideOnForm(),
            DateTimeField::new('fechaEntrega', 'Fecha de la entrega')
                ->hideOnForm(),
            AssociationField::new('persona', 'Clientes')
                ->setQueryBuilder(
                    fn(QueryBuilder $queryBuilder) => $queryBuilder
                        ->select('persona')
                        ->distinct()
                        ->from(Persona::class, 'persona')
                        ->join('persona.pedidos', 'pedidos')
                        ->where('pedidos.seEntrego = :seEntrego')
                        ->setParameter('seEntrego', false)
                        ->orderBy('persona.nombre', 'ASC') // Cambia a tu campo de ordenación deseado
                )
                ->onlyWhenCreating(),
            AssociationField::new('persona', 'Clientes')
                ->hideWhenCreating()
                ->setDisabled(),
            AssociationField::new('monto', 'Importe')
                ->renderAsEmbeddedForm()
                ->setCrudController(MontoCrudController::class)
                ->onlyOnForms(),
            TextareaField::new('nota', 'Observaciones')
                ->hideOnIndex()
                ->setFormTypeOption('attr', ['placeholder' => 'Escriba algún comentario...', 'class' => 'text-ligth']),
            CollectionField::new('detalleEntregas', 'Detalles')
                ->useEntryCrudForm(DetalleEntregaCrudController::class)
                ->onlyWhenUpdating(),
            BooleanField::new('seEntrego', 'Entregado')
                ->renderAsSwitch(false)
                ->hideOnForm(),
            BooleanField::new('sePago', 'Cobrado')
                ->renderAsSwitch(false)
                ->hideOnForm(),
            TextField::new('totalFormateado', 'Importe')
                ->formatValue(function ($monto, Entrega $entity) {
                    return $this->principalService->formatearMonto($entity->getMonto());
                })
                ->addCssClass('text-success')
                ->onlyOnIndex(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $botonVer = Action::new('app_entrega_show', 'Ver', 'fa fa-eye')
            ->linkToRoute('app_entrega_show', function (Entrega $entrega): array {
                return [
                    'id' => $entrega->getId(),
                ];
            });
        $botonEntregar = Action::new('app_entrega_entregar', 'Entregar', 'fa fa-eye')
            ->linkToRoute('app_entrega_entregar', function (Entrega $entrega): array {
                return [
                    'id' => $entrega->getId()
                ];
            });
        $botonCobrar = Action::new('app_entrega_cobrar', 'Pagar', 'fa fa-eye')
            ->linkToRoute('app_entrega_cobrar', function (Entrega $entrega): array {
                return [
                    'id' => $entrega->getId()
                ];
            });
        $goToStripe = Action::new('Agregar')
            ->linkToRoute('app_entrega_new')
            ->createAsGlobalAction();
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action
                    ->setIcon('fa fa-pencil')
                    ->setLabel('Editar')
                    ->displayIf(static function (Entrega $entity) {
                        return !$entity->isSeEntrego();
                    }))
            ->add(Crud::PAGE_INDEX, $botonVer)
            ->add(Crud::PAGE_INDEX, $botonEntregar)
            ->update(Crud::PAGE_INDEX, $botonEntregar,
                fn(Action $action) => $action
                    ->displayIf(static function (Entrega $entity) {
                        return !$entity->isSeEntrego();
                    }))
            ->add(Crud::PAGE_INDEX, $botonCobrar)
            ->update(Crud::PAGE_INDEX, $botonCobrar,
                fn(Action $action) => $action
                    ->displayIf(static function (Entrega $entity) {
                        return $entity->isSeEntrego() && !$entity->isSePago();
                    }))
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action
                    ->setIcon('fa fa-globe')
                    ->setLabel('Editar'))
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function getRedirectResponseAfterSave(AdminContext $context, string $action): RedirectResponse
    {
        $submitButtonName = $context->getRequest()->request->all()['ea']['newForm']['btn'];
        if ('saveAndReturn' === $submitButtonName) {
//            if ($action == 'new')
            $url = ($action == 'new')
                ? $this->container->get(AdminUrlGenerator::class)
                    ->setAction(Action::EDIT)
                    ->setEntityId($context->getEntity()->getPrimaryKeyValue())
                    ->generateUrl()
                : $this->container->get(AdminUrlGenerator::class)
                    ->setRoute('app_entrega_show', [
                        'id' => $context->getEntity()->getPrimaryKeyValue(),
                    ])
                    ->generateUrl();
            return $this->redirect($url);
        }
        return parent::getRedirectResponseAfterSave($context, $action);
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Entregas')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar entrega')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear entrega')
            ->showEntityActionsInlined()
            ->setDefaultSort(['seEntrego' => 'ASC', 'sePago' => 'ASC', 'fechaEntrega' => 'DESC']);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('persona')
            ->add('seEntrego')
            ->add('sePago');
    }
}
