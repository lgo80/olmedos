<?php

namespace App\Controller\Admin;

use App\Entity\Ciudad;
use App\Entity\ClasificacionDeBobinas;
use App\Entity\Compra;
use App\Entity\Direccion;
use App\Entity\Entrega;
use App\Entity\ListaDePrecios;
use App\Entity\MateriaPrima;
use App\Entity\Moneda;
use App\Entity\Pago;
use App\Entity\Pais;
use App\Entity\Pedido;
use App\Entity\Persona;
use App\Entity\Producto;
use App\Entity\Provincia;
use App\Entity\Transaccion;
use App\Entity\User;
use App\Entity\Venta;
use App\Repository\MonedaRepository;
use App\Service\Utils\PrincipalService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private readonly PrincipalService $principalService,
        private readonly MonedaRepository $monedaRepository
    )
    {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(PedidoCrudController::class)->generateUrl());
    }

    public function configureAssets(): Assets
    {
        return parent::configureAssets()
            ->addWebpackEncoreEntry('app')
            ->addWebpackEncoreEntry('producto')
            ->addWebpackEncoreEntry('caja')
            ->addWebpackEncoreEntry('entrega')
            ->addWebpackEncoreEntry('transaccion')
            ->addWebpackEncoreEntry('pedido');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Olmedos')
            ->setFaviconPath('favicon.svg')
            ->setTranslationDomain('Olmedos')
            ->setTextDirection('ltr');
    }

    public function configureMenuItems(): iterable
    {
        $signo = $this->getUser()->getMoneda()->getSigno();
        $monedaDolar = $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ]);
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::subMenu('Altas mas usadas', 'fa fa-thermometer-full')->setSubItems([
            MenuItem::linkToCrud('Pedidos', 'fa fa-shopping-basket', Pedido::class),
            MenuItem::linkToCrud('Entregas', 'fa fa-truck', Entrega::class),
            MenuItem::linktoRoute('Movimientos de caja', 'fa fa-balance-scale', 'app_admin_caja'),
            MenuItem::linkToCrud('Compras', 'fa fa-money', Compra::class),
            MenuItem::linkToCrud('Pagos', 'fa fa-university', Pago::class),
            MenuItem::linkToCrud('Transacciones', 'fa fa-calculator', Transaccion::class),
            MenuItem::linkToCrud('Lista de precios', 'fa fa-usd', ListaDePrecios::class),
            MenuItem::linkToCrud('Ventas', 'fa fa-cubes', Venta::class)

        ]);
        yield MenuItem::subMenu('Altas medio frecuentes', 'fa fa-thermometer-half')->setSubItems([
            MenuItem::linkToCrud('Productos', 'fa fa-product-hunt', Producto::class),
            MenuItem::linkToCrud('Personas', 'fa fa-user-circle-o', Persona::class),
            MenuItem::linkToCrud('Materia Prima', 'fa fa-cubes', MateriaPrima::class)
        ]);
        yield MenuItem::subMenu('Altas poco frecuentes', 'fa fa-thermometer-empty')->setSubItems([
            MenuItem::linkToCrud('Usuarios', 'fa fa-users', User::class),
            MenuItem::linkToCrud('Países', 'fa fa-globe', Pais::class),
            MenuItem::linkToCrud('Provincias', 'fa fa-map-o', Provincia::class),
            MenuItem::linkToCrud('Ciudades', 'fa fa-arrows-alt', Ciudad::class),
            MenuItem::linkToCrud('Direcciones', 'fa fa-address-card', Direccion::class),
            MenuItem::linkToCrud('Clasificacion de las bobinas', 'fa fa-indent', ClasificacionDeBobinas::class),
            MenuItem::linkToCrud('Monedas', 'fa fa-money', Moneda::class)
        ]);
        yield MenuItem::linkToCrud('Tipo de cambio: '
            . $this->principalService->formatearNumero(
                $monedaDolar->getTipoCambio(), false, '$'), 'fa fa-money', Moneda::class)
            ->setAction('edit')
            ->setEntityId(2);
        yield MenuItem::linkToCrud('Moneda usada:  ' . $signo, 'fa fa-money', User::class)
            ->setAction('edit')
            ->setEntityId($this->getUser()->getId());
    }

}
