<?php

namespace App\Controller\Admin;

use App\Entity\Venta;
use App\Service\Utils\VentaService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Utils\PrincipalService;

class VentaController extends AbstractController
{
    #[Route('/admin/venta/show/{id}', name: 'app_venta_show')]
    public function show(
        Venta            $venta,
        PrincipalService $principalService,
        VentaService     $ventaService,
    ): Response
    {
        return $this->render('admin/venta/show.html.twig', [
            'venta' => $venta,
            'service' => $principalService,
            'MontoVenta' => $ventaService->getImporteDeLaEntrega($venta)
        ]);
    }
}
