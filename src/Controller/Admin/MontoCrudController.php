<?php

namespace App\Controller\Admin;

use App\Entity\Compra;
use App\Entity\Entrega;
use App\Entity\Monto;
use App\Entity\Pedido;
use App\Entity\Transaccion;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class MontoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Monto::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $entity = $this->getContext()->getEntity()->getFqcn();
        if ($entity === Transaccion::class)
            return [
                NumberField::new('importe', 'Importe'),
                AssociationField::new('moneda', 'Moneda'),
                ChoiceField::new('base', 'Base')
                    ->setChoices([
                        Monto::BASE_SUMA => 1,
                        Monto::BASE_RESTA => -1,
                    ])
                    ->setColumns('col-12')
                    ->setFormTypeOption('attr', ['class' => 'baseImporte']),
            ];
        elseif ($entity === Compra::class || $entity === Pedido::class || $entity === Entrega::class)
            return [
                AssociationField::new('moneda', 'Moneda')
                    ->setColumns('col-12'),
            ];
        else
            return [
                NumberField::new('importe', 'Importe')
                    ->setColumns('col-12'),
                AssociationField::new('moneda', 'Moneda')
                    ->setColumns('col-12'),
            ];
    }
}
