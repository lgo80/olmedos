<?php

namespace App\Controller\Admin;

use App\Entity\Compra;
use App\Service\Utils\PrincipalService;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CompraCrudController extends AbstractCrudController
{
    public function __construct(
        private readonly PrincipalService $principalService
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Compra::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')
                ->hideOnForm(),
            DateTimeField::new('fecha', 'Fecha de compra'),
            AssociationField::new('proveedor', 'Proveedor'),
            AssociationField::new('monto', 'Importe')
                ->renderAsEmbeddedForm()
                ->setCrudController(MontoCrudController::class)
                ->onlyOnForms(),
            TextAreaField::new('nota', 'Observaciones')
                ->hideOnIndex(),
            CollectionField::new('detalleCompras', 'Detalles')
                ->useEntryCrudForm(DetalleCompraCrudController::class)
                ->onlyOnForms(),
            TextField::new('montoEnStr', 'Importe')
                ->formatValue(function ($monto, Compra $entity) {
                    return $this->principalService->formatearMonto($entity->getMonto());
                })
                ->addCssClass('text-danger')
                ->onlyOnIndex(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('proveedor')
            ->add('fecha');
    }

    public function configureActions(Actions $actions): Actions
    {
        $sendInvoice = Action::new('app_compras', 'Ver', 'fa fa-eye')
            ->linkToRoute('app_compras', function (Compra $compra): array {
                return [
                    'id' => $compra->getId(),
                ];
            });
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, $sendInvoice)
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->update(Crud::PAGE_DETAIL, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'));
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Compras')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar compra')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear compra')
            ->setPageTitle(Crud::PAGE_DETAIL, 'Ver compra')
            ->showEntityActionsInlined()
            ->setDefaultSort(['fecha' => 'DESC']);
    }
}
