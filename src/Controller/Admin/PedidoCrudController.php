<?php

namespace App\Controller\Admin;

use App\Entity\Pedido;
use App\Entity\Persona;
use App\Service\Utils\PrincipalService;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;

class PedidoCrudController extends AbstractCrudController
{

    public function __construct(
        private readonly PrincipalService $principalService
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Pedido::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')->hideOnForm(),
            DateTimeField::new('fechaAt', 'Fecha del pedido'),
            AssociationField::new('persona', 'Cliente')
                ->setQueryBuilder(
                    fn(QueryBuilder $queryBuilder) => $queryBuilder
                        ->select('persona')
                        ->distinct()
                        ->from(Persona::class, 'persona')
                        ->where('persona.tipo = :tipo1')
                        ->orWhere('persona.tipo = :tipo2')
                        ->setParameter('tipo1', Persona::TIPO_CLIENTE)
                        ->setParameter('tipo2', Persona::TIPO_AMBOS)
                        ->orderBy('persona.nombre', 'ASC')
                ),
            AssociationField::new('monto', 'Importe')
                ->renderAsEmbeddedForm()
                ->setCrudController(MontoCrudController::class)
                ->onlyOnForms(),
            TextareaField::new('nota', 'Observaciones')
                ->hideOnIndex()
                ->addCssClass('text-danger'),
            CollectionField::new('detallePedidos', 'Detalles')
                ->useEntryCrudForm(DetallePedidoCrudController::class)
                ->hideOnIndex(),
            BooleanField::new('seEntrego', 'Entregado')
                ->renderAsSwitch(false)
                ->hideOnForm(),
            TextField::new('totalFormateado', 'Importe')
                ->formatValue(function ($monto, Pedido $entity) {
                    return $this->principalService->formatearMonto($entity->getMonto());
                })
                ->addCssClass('text-success')
                ->onlyOnIndex(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        $sendInvoice = Action::new('app_pedido_show', 'Ver', 'fa fa-eye')
            ->linkToRoute('app_pedido_show', function (Pedido $pedido): array {
                return [
                    'id' => $pedido->getId(),
                ];
            });
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action
                    ->setIcon('fa fa-pencil')
                    ->setLabel('Editar')
                    ->displayIf(static function (Pedido $entity) {
                        return !$entity->isSeEntrego();
                    }))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, $sendInvoice)
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'));

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Pedidos')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar pedido')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear pedido')
            ->showEntityActionsInlined()
            ->setDefaultSort(['seEntrego' => 'ASC', 'fechaAt' => 'DESC']);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('persona')
            ->add('seEntrego');
    }

}
