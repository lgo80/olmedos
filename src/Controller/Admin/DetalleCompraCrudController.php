<?php

namespace App\Controller\Admin;

use App\Entity\DetalleCompra;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;

class DetalleCompraCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DetalleCompra::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('materiaPrima', 'Materia Prima')
                ->onlyOnForms()
                ->setColumns('col-12'),
            NumberField::new('precio', 'Precio por unidad')
                ->setColumns('col-12'),
            IntegerField::new('cantidad', 'Cantidad')
                ->setColumns('col-12')
        ];
    }
}
