<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;

class UserCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return User::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')
                ->hideOnForm(),
            EmailField::new('email', 'Email'),
            DateTimeField::new('dateCreatedAt', 'Fecha de creacion')
                ->hideOnForm(),
            BooleanField::new('isVerified', 'Esta verificado')
                ->renderAsSwitch(false)
                ->hideOnForm(),
            AssociationField::new('moneda', 'Moneda elegida'),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL,
                fn(Action $action) => $action->setIcon('fa fa-eye')->setLabel('Ver'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->update(Crud::PAGE_DETAIL, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_DETAIL, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'));
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Usuarios')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar usuario')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear usuario')
            ->showEntityActionsInlined()
            ->setDefaultSort(['email' => 'ASC']);
    }
}
