<?php

namespace App\Controller\Admin;

use App\Service\Utils\CajaService;
use App\Service\Utils\PrincipalService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CajaController extends AbstractController
{

    /**
     * @throws Exception
     */
    #[Route('/admin/caja', name: 'app_admin_caja', methods: ['GET', 'POST'])]
    public function index(
        CajaService      $cajaService,
        Request          $request,
        PrincipalService $principalService
    ): Response
    {
        [$cajas, $error] = ($cajaService)($request);
        return $this->render('admin/caja/index.html.twig',
            [
                'cajas' => $cajas['cajas'],
                'fechadesde' => $cajas['fechadesde'],
                'fechahasta' => $cajas['fechahasta'],
                'orden' => $cajas['orden'],
                'total' => $cajas['total'],
                'principalService' => $principalService
            ]
        );
    }
}
