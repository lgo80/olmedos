<?php

namespace App\Controller\Admin;

use App\Entity\DetallePedido;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class DetallePedidoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return DetallePedido::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('producto', 'Producto')
                ->onlyOnForms()
                ->setFormTypeOption('attr', ['class' => 'classProductoPedido'])
                ->setColumns('col-12'),
            NumberField::new('precio', 'Precio por unidad')
                ->setFormTypeOption('attr', ['class' => 'classPrecioPedido'])
                ->setColumns('col-12'),
            IntegerField::new('cantidad', 'Cantidad')
                ->setColumns('col-12'),
            TextareaField::new('nota', 'Observaciones')
                ->addCssClass('text-danger')
                ->setColumns('col-12'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['producto.detalle' => 'ASC', 'producto.diametro' => 'ASC', 'producto.bobinado' => 'ASC', 'producto.impedancia' => 'ASC', 'producto.material' => 'ASC', 'producto.potencia' => 'ASC']);
    }
}
