<?php

namespace App\Controller\Admin;

use App\Entity\Direccion;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class DireccionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Direccion::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')
                ->setColumns('col-12')
                ->hideOnForm(),
            TextField::new('calle')
                ->setColumns('col-12')
                ->onlyOnForms(),
            IntegerField::new('numero')
                ->setColumns('col-12')
                ->onlyOnForms(),
            IntegerField::new('piso')
                ->setColumns('col-12')
                ->onlyOnForms(),
            IntegerField::new('departamento')
                ->setColumns('col-12')
                ->onlyOnForms(),
            IntegerField::new('codigoPostal')
                ->setColumns('col-12')
                ->onlyOnForms(),
            AssociationField::new('ciudad')
                ->setColumns('col-12')
                ->onlyOnForms(),
            TextField::new('nombreCompleto', 'Nombre completo')
                ->setColumns('col-12')
                ->onlyOnIndex(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'));
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Direcciones')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar dirección')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear dirección')
            ->showEntityActionsInlined()
            ->setDefaultSort(['calle' => 'ASC', 'numero' => 'ASC', 'ciudad' => 'ASC']);
    }
}
