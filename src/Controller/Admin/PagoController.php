<?php

namespace App\Controller\Admin;

use App\Entity\Pago;
use App\Repository\PagoRepository;
use App\Service\Utils\PrincipalService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagoController extends AbstractController
{
    #[Route('/admin/pago', name: 'app_pago_ver_sueldo')]
    public function verSueldo(
        Request          $request,
        PrincipalService $principalService,
        PagoRepository   $pagoRepository
    ): Response
    {
        $respuesta = ($request->request->get('cmbPersona'))
            ? $this->getPagos($request, $pagoRepository)
            : $this->obtenerListaDeSueldos($principalService, $pagoRepository);
        return $this->render($respuesta['view'], [
            'pagos' => $respuesta['pagos'] ?? null,
            'principalService' => $principalService,
            'sueldo' => $respuesta['sueldo'] ?? 0,
        ]);
    }

    private function getPagos(
        Request        $request,
        PagoRepository $pagoRepository
    ): ?array
    {
        $persona = $request->request->get('cmbPersona');
        $periodo = $request->request->get('txtPeriodo');
        if ((!$persona || $persona === 'Seleccione...') || !$periodo)
            return null;
        $pagos = $pagoRepository->findBy(['sujeto' => $persona]);
        $pagos = array_filter($pagos, fn(Pago $pago) => $pago
            ->estaDentroDelPeriodo($periodo));
        $sueldo = $request->request->get('txtSueldo');
        return [
            'pagos' => $pagos,
            'sueldo' => is_numeric($sueldo) && $sueldo > 0 ? $sueldo : 0,
            'view' => 'admin/pago/index.html.twig',
        ];
    }

    private function obtenerListaDeSueldos(
        PrincipalService $principalService,
        PagoRepository   $pagoRepository
    ): ?array
    {
        $retornar = [];
        for ($i = 4; $i >= 1; $i--) {
            $date = new DateTime(); // Fecha actual
            $date->modify('-' . $i . 'months');
            $pagos = $pagoRepository->findBy(['sujeto' => 1]);
            $pagos = array_filter($pagos, fn(Pago $pago) => $pago
                ->estaDentroDelPeriodo($date->format('Y-m-d\TH:i')));
            $pagos2 = $pagoRepository->findBy(['sujeto' => 2]);
            $pagos2 = array_filter($pagos2, fn(Pago $pago) => $pago
                ->estaDentroDelPeriodo($date->format('Y-m-d\TH:i')));
            $retornar[] = [
                "periodo" => $date->format('Y-m-d\TH:i'),
                "leo" => $this->obtenerMontoDelMes($pagos, $principalService),
                "mariano" => $this->obtenerMontoDelMes($pagos2, $principalService)
            ];
        }
        return [
            'pagos' => $retornar,
            'sueldo' => 0,
            'view' => 'admin/pago/indexPrincipal.html.twig',
        ];
    }

    private function obtenerMontoDelMes(
        array            $pagos,
        PrincipalService $principalService
    ): float
    {
        $suma = 0;
        /** @var Pago $pago */
        foreach ($pagos as $pago) {
            $suma += $principalService->devolverImporteInt($pago->getMonto());//devolverImporteInt(pago.monto)
        }
        return $suma;
    }
}
