<?php

namespace App\Controller\Admin;

use App\Entity\Persona;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PersonaCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Persona::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')->hideOnForm(),
            ChoiceField::new('tipo', 'Tipo')->setChoices([
                'Cliente' => 1,
                'Proveedor' => 2,
                'Ambos' => 3
            ]),
            TextField::new('razonSocial', 'Razon social'),
            TextField::new('nombre', 'Nombre'),
            TextField::new('email', 'Correo electrónico'),
            TextField::new('cuit', 'Cuit')
                ->hideOnIndex(),
            TextField::new('telefono', 'Teléfono'),
            TextareaField::new('observacion', 'Observacion')
                ->hideOnIndex()
            ,
            BooleanField::new('isActivo', 'Esta activo')
                ->renderAsSwitch(false),
            CollectionField::new('direcciones', 'Cargar direcciones')
                ->useEntryCrudForm(DireccionCrudController::class)
                ->setColumns('col-8')
                ->onlyOnForms(),
            TextField::new('getDireccionCompleta', 'Dirección')
                ->onlyOnDetail(),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL,
                fn(Action $action) => $action->setIcon('fa fa-eye')->setLabel('Ver'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_DETAIL, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->update(Crud::PAGE_DETAIL, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_DETAIL, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'));
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Personas')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar persona')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear persona')
            ->showEntityActionsInlined()
            ->setDefaultSort(['razonSocial' => 'ASC']);
    }
}
