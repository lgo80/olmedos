<?php

namespace App\Controller\Admin;

use App\Entity\Pedido;
use App\Repository\ListaDePreciosRepository;
use App\Repository\ProductoRepository;
use App\Repository\TransaccionRepository;
use App\Service\Utils\ListaDePreciosService;
use App\Service\Utils\PrincipalService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class PedidoController extends AbstractController
{
    #[Route('/admin/pedido/show/{id}', name: 'app_pedido_show')]
    public function show(
        Pedido                $pedido,
        TransaccionRepository $transaccionRepository,
        PrincipalService      $principalService): Response
    {
        if (!$pedido->isSeEntrego())
            $transacciones = $transaccionRepository->findBy([
                'persona' => $pedido->getPersona(),
                'estaCobrado' => false
            ]);
        return $this->render('admin/pedido/show.html.twig', [
            'pedido' => $pedido,
            'transacciones' => $transacciones ?? null,
            'principalService' => $principalService,
        ]);
    }

    #[Route('/admin/devolverprecio/', name: 'app_pedido_devolver_precio')]
    public function devolverPrecio(
        Request                  $request,
        ProductoRepository       $productoRepository,
        PrincipalService         $principalService,
        ListaDePreciosService    $listaDePreciosService,
        ListaDePreciosRepository $listaDePreciosRepository
    ): JsonResponse
    {
        $idProducto = $request->request->get('idProducto');
        $idPersona = $request->request->get('idPersona');
        $producto = $productoRepository->find($idProducto);
        $listaDePreciosDelProducto = $listaDePreciosRepository->findBy([
            'tipo' => $producto->getTipoProducto()
        ]);
        $listaDePrecio = $listaDePreciosService
            ->getListaDePrecioDelaPersona($listaDePreciosDelProducto, $idPersona);
        if (!$listaDePrecio)
            return new JsonResponse(['precio' => 0]);
        $montoProducto = $listaDePrecio
            ->getPrecioProducto($producto);
        return new JsonResponse(['precio' => $montoProducto ? $principalService
            ->devolverImporteInt($montoProducto) : 0]);
    }
}
