<?php

namespace App\Controller\Admin;

use App\Entity\DetalleEntrega;
use App\Entity\DetallePedido;
use App\Repository\EntregaRepository;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use JetBrains\PhpStorm\Pure;

class DetalleEntregaCrudController extends AbstractCrudController
{

    #[Pure] public function __construct(
        private readonly EntregaRepository $entregaRepository,
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return DetalleEntrega::class;
    }

    public function configureFields(string $pageName): iterable
    {
        $idEntrega = $this->getContext()->getRequest()->get('entityId');
        $entrega = $this->entregaRepository->find($idEntrega);
        return [
            AssociationField::new('detallePedido', 'Detalles')
                ->setQueryBuilder(
                    function (QueryBuilder $queryBuilder) use ($entrega) {
                        $queryBuilder
                            ->select('detallePedidos')
                            ->from(DetallePedido::class, 'detallePedidos')
                            ->join('detallePedidos.pedido', 'pedido')
                            ->join('pedido.persona', 'persona')
                            ->where('pedido.seEntrego = :seEntrego')
                            ->andWhere('persona.id = :personaId')
                            ->setParameter('seEntrego', false)
                            ->setParameter('personaId', $entrega->getPersona()->getId())
                            ->orderBy('detallePedidos.id', 'ASC'); // Reemplaza 'algunCampo' con el campo que desees ordenar
                    })
                ->onlyOnForms()
                ->setFormTypeOption('attr', ['class' => 'classDetallePedidoEntrega'])
                ->setColumns('col-12'),
            NumberField::new('precio', 'Precio por unidad')
                ->setColumns('col-12')
                ->setFormTypeOption('attr', ['class' => 'classPrecioEntrega']),
            IntegerField::new('cantidad', 'Cantidad')
                ->setColumns('col-12'),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['producto.detalle' => 'ASC', 'producto.diametro' => 'ASC', 'producto.bobinado' => 'ASC', 'producto.impedancia' => 'ASC', 'producto.material' => 'ASC', 'producto.potencia' => 'ASC']);
    }
}
