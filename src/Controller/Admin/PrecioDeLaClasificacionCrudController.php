<?php

namespace App\Controller\Admin;

use App\Entity\PrecioDeLaClasificacion;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class PrecioDeLaClasificacionCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PrecioDeLaClasificacion::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('clasificacionDeBobinas', 'Clasificaciónn de la bobina')
                ->onlyOnForms()
                ->setFormTypeOption('attr', ['class' => 'classClasificacionBobina'])
                ->setColumns('col-12'),
            AssociationField::new('monto', 'Importe')
                ->renderAsEmbeddedForm()
                ->setCrudController(MontoCrudController::class)
                ->onlyOnForms()
                ->setColumns('col-12'),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
