<?php

namespace App\Controller\Admin;

use App\Entity\ListaDePrecios;
use App\Form\ListaPreciosPersonaType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ListaDePreciosCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ListaDePrecios::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id', 'Identificador')->hideOnForm(),
            TextField::new('nombre', 'Nombre'),
            ChoiceField::new('tipo', 'Tipo de lista')->setChoices([
                'Bobina' => 1,
                'Parlante' => 2,
                'Reparación' => 3
            ]),
            CollectionField::new('preciosDeLasClasificaciones', 'Precios de las clasificaciones')
                ->useEntryCrudForm(PrecioDeLaClasificacionCrudController::class)
                ->onlyOnForms(),
            CollectionField::new('preciosProductosSinClasificars', 'Precios de los productos sin clasificar')
                ->useEntryCrudForm(PrecioProductoSinClasificarCrudController::class)
                ->onlyOnForms(),
            CollectionField::new('listaPreciosPersonas', 'Personas de la lista')
                ->setEntryType(ListaPreciosPersonaType::class),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function configureActions(Actions $actions): Actions
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        $url = $adminUrlGenerator
            ->setController(ListaDePreciosController::class)
            ->setAction('aumentar')
            ->generateUrl();
        $goToStripe = Action::new('app_aumentar_lista_de_precios', 'Aumentar precios', 'fa fa-line-chart')
            ->linkToRoute('app_aumentar_lista_de_precios')
            ->createAsGlobalAction();
        $sendInvoice = Action::new('app_show_lista_de_precios', 'Ver', 'fa fa-eye')
            ->linkToRoute('app_show_lista_de_precios', function (ListaDePrecios $listaDePrecios): array {
                return [
                    'id' => $listaDePrecios->getId(),
                ];
            });
        return $actions
            ->add(Crud::PAGE_INDEX, $goToStripe)
            ->update(Crud::PAGE_INDEX, Action::NEW,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Agregar'))
            ->update(Crud::PAGE_INDEX, Action::EDIT,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Editar'))
            ->update(Crud::PAGE_INDEX, Action::DELETE,
                fn(Action $action) => $action->setIcon('fa fa-trash-o')->setLabel('Eliminar'))
            ->add(Crud::PAGE_INDEX, $sendInvoice)
            ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-globe')->setLabel('Editar'))
            ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->update(Crud::PAGE_EDIT, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN,
                fn(Action $action) => $action->setIcon('fa fa-pencil')->setLabel('Guardar'))
            ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER,
                fn(Action $action) => $action->setIcon('fa fa-plus-circle')->setLabel('Guardar y crear otro'))
            ->add(Crud::PAGE_NEW, Action::INDEX)
            ->update(Crud::PAGE_NEW, Action::INDEX,
                fn(Action $action) => $action->setIcon('fa fa-undo')->setLabel('Volver a la lista'));
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle(Crud::PAGE_INDEX, 'Lista de precios')
            ->setPageTitle(Crud::PAGE_EDIT, 'Editar lista')
            ->setPageTitle(Crud::PAGE_NEW, 'Crear lista')
            ->setPageTitle(Crud::PAGE_DETAIL, 'Ver lista')
            ->showEntityActionsInlined();
    }
}
