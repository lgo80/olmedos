<?php

namespace App\Controller\Admin;

use App\Entity\PrecioProductoSinClasificar;
use App\Entity\Producto;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class PrecioProductoSinClasificarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return PrecioProductoSinClasificar::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('producto', 'Producto')
                ->setQueryBuilder(
                    fn(QueryBuilder $queryBuilder) => $queryBuilder
                        ->select('producto')
                        ->distinct()
                        ->from(Producto::class, 'producto')
                        ->where('producto.tipoProducto > :tipoProducto')
                        ->setParameter('tipoProducto', 1)
                        ->orderBy('producto.detalle', 'ASC') // Cambia a tu campo de ordenación deseado
                )
                ->onlyOnForms()
                ->setFormTypeOption('attr', ['class' => 'classProductoSinClasificar'])
                ->setColumns('col-12'),
            AssociationField::new('monto', 'Importe')
                ->renderAsEmbeddedForm()
                ->setCrudController(MontoCrudController::class)
                ->onlyOnForms()
                ->setColumns('col-12'),
            DateTimeField::new('dateCreatedAt', 'Fecha de creación')
                ->onlyOnDetail(),
            DateTimeField::new('dateUpdatedAt', 'Última modificaciòón')
                ->onlyOnDetail()
        ];
    }
}
