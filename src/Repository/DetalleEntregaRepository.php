<?php

namespace App\Repository;

use App\Entity\DetalleEntrega;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DetalleEntrega>
 *
 * @method DetalleEntrega|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetalleEntrega|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetalleEntrega[]    findAll()
 * @method DetalleEntrega[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetalleEntregaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetalleEntrega::class);
    }

//    /**
//     * @return DetalleEntrega[] Returns an array of DetalleEntrega objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DetalleEntrega
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
