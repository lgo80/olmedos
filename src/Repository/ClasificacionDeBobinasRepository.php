<?php

namespace App\Repository;

use App\Entity\ClasificacionDeBobinas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClasificacionDeBobinas>
 *
 * @method ClasificacionDeBobinas|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClasificacionDeBobinas|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClasificacionDeBobinas[]    findAll()
 * @method ClasificacionDeBobinas[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClasificacionDeBobinasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClasificacionDeBobinas::class);
    }

//    /**
//     * @return ClasificacionDeBobinas[] Returns an array of ClasificacionDeBobinas objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ClasificacionDeBobinas
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
