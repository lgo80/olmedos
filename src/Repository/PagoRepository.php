<?php

namespace App\Repository;

use App\Entity\Pago;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Pago>
 *
 * @method Pago|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pago|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pago[]    findAll()
 * @method Pago[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PagoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pago::class);
    }

    /*
     * @return Pago[]
    public function findAllGreaterThanPrice(int $price): array
    {
        $entityManager = $this->getEntityManager();
                 * SELECT Pago.tipo,Pago.sujeto,DATE_FORMAT(Pago.periodo, '%Y%m') AS yearMonth,SUM(Monto.importe) FROM Pago,Monto
                 *  WHERE Pago.tipo = 1 AND Pago.sujeto = 1 AND Pago.monto_id = Monto.id GROUP BY tipo,sujeto,yearMonth;
        $query = $entityManager->createQuery(
            'SELECT p.tipo,p.sujeto,DATE_FORMAT(p.periodo, '%Y%m') AS yearMonth,SUM(m.importe)
            FROM App\Entity\Monto m, App\Entity\Pago p
            WHERE p.price > :price
            ORDER BY p.price ASC'
        )->setParameter('price', $price);

        // returns an array of Product objects
        return $query->getResult();
    }
*/

//    /**
//     * @return Pago[] Returns an array of Pago objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Pago
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
