<?php

namespace App\Repository;

use App\Entity\PrecioDeLaClasificacion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PrecioDeLaClasificacion>
 *
 * @method PrecioDeLaClasificacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrecioDeLaClasificacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrecioDeLaClasificacion[]    findAll()
 * @method PrecioDeLaClasificacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrecioDeLaClasificacionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrecioDeLaClasificacion::class);
    }

//    /**
//     * @return PreciosDeLasClasificaciones[] Returns an array of PreciosDeLasClasificaciones objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PreciosDeLasClasificaciones
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
