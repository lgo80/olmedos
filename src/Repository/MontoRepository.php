<?php

namespace App\Repository;

use App\Entity\Monto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Monto>
 *
 * @method Monto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Monto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Monto[]    findAll()
 * @method Monto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MontoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Monto::class);
    }

    public function save(
        Monto $monto,
        bool  $flush = false): void
    {
        $this->getEntityManager()->persist($monto);
        if ($flush)
            $this->getEntityManager()->flush();
    }

//    /**
//     * @return Monto[] Returns an array of Monto objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('m.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Monto
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
