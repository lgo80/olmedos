<?php

namespace App\Repository;

use App\Entity\ListaPreciosPersona;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ListaPreciosPersona>
 *
 * @method ListaPreciosPersona|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListaPreciosPersona|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListaPreciosPersona[]    findAll()
 * @method ListaPreciosPersona[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListaPreciosPersonaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListaPreciosPersona::class);
    }

//    /**
//     * @return ListaPreciosPersona[] Returns an array of ListaPreciosPersona objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('l.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ListaPreciosPersona
//    {
//        return $this->createQueryBuilder('l')
//            ->andWhere('l.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
