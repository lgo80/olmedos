<?php

namespace App\Repository;

use App\Entity\Pedido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Pedido>
 *
 * @method Pedido|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pedido|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pedido[]    findAll()
 * @method Pedido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PedidoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Pedido::class);
    }

    public function save(
        Pedido $pedido,
        bool   $flush = false): void
    {
        $this->getEntityManager()->persist($pedido);
        if ($flush)
            $this->getEntityManager()->flush();
    }

    public function remove(
        Pedido $pedido,
        bool   $flush = false): void
    {
        $this->getEntityManager()->remove($pedido);
        if ($flush)
            $this->getEntityManager()->flush();
    }
}
