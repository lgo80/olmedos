<?php

namespace App\Repository;

use App\Entity\Entrega;
use App\Entity\Pedido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Entrega>
 *
 * @method Entrega|null find($id, $lockMode = null, $lockVersion = null)
 * @method Entrega|null findOneBy(array $criteria, array $orderBy = null)
 * @method Entrega[]    findAll()
 * @method Entrega[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EntregaRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry)
    {
        parent::__construct($registry, Entrega::class);
    }

    public function save(
        Entrega $entrega,
        bool    $flush = false): void
    {
        $this->getEntityManager()->persist($entrega);
        if ($flush)
            $this->getEntityManager()->flush();
    }
    /*
        public function beforeSave(
            Entrega $entrega): void
        {
            if (!$entrega->getMonto()->getId()) {
                $monto = $entrega->getMonto();
                $this->montoRepository->save($monto, true);
                $entrega->setMonto($monto);
                dd($monto, $entrega);
            }
            $this->save($entrega, true);
        }*/

    /**
     * @return Entrega[] Returns an array of Entrega objects
     */
    public function findByExampleField(Pedido $value): array
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.pedido = :val')
            ->setParameter('val', $value)
            ->innerJoin()
            ->getQuery()
            ->getResult();
    }

}
