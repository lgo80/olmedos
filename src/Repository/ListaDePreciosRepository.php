<?php

namespace App\Repository;

use App\Entity\ListaDePrecios;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ListaDePrecios>
 *
 * @method ListaDePrecios|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListaDePrecios|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListaDePrecios[]    findAll()
 * @method ListaDePrecios[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListaDePreciosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListaDePrecios::class);
    }

    public function save(
        ListaDePrecios $listaDePrecios,
        bool           $flush = false): void
    {
        $this->getEntityManager()->persist($listaDePrecios);
        if ($flush)
            $this->getEntityManager()->flush();
    }

    public function update(
        ListaDePrecios $listaDePrecios): void
    {
//        $this->getEntityManager()->persist($listaDePrecios);
        $this->getEntityManager()->flush();
    }

}
