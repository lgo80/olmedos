<?php

namespace App\Repository;

use App\Entity\Caja;
use App\Entity\Compra;
use App\Entity\Pago;
use App\Entity\Transaccion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Caja>
 *
 * @method Caja|null find($id, $lockMode = null, $lockVersion = null)
 * @method Caja|null findOneBy(array $criteria, array $orderBy = null)
 * @method Caja[]    findAll()
 * @method Caja[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CajaRepository extends ServiceEntityRepository
{
    public function __construct(
        ManagerRegistry $registry
    )
    {
        parent::__construct($registry, Caja::class);
    }

    public function save(
        Caja $caja,
        bool $flush = false): void
    {
        $this->getEntityManager()->persist($caja);

        if ($flush)
            $this->getEntityManager()->flush();
    }

    public function grabarGasto(Compra|Pago|Transaccion|null $entidad): void
    {
        $caja = new Caja();
        if ($entidad instanceof Compra)
            $caja->setCompra($entidad);
        else if ($entidad instanceof Pago)
            $caja->setPago($entidad);
        else if ($entidad instanceof Transaccion
            && $entidad->getTipo() == Transaccion::TIPO_ANTICIPO)
            $caja->setTransaccion($entidad);
        else
            return;
        $this->save($caja, true);
    }

    /**
     * @return Caja[] Returns an array of Caja objects
     */
    public function findByExampleField($value): array
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

//    public function findOneBySomeField($value): ?Caja
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
