<?php

namespace App\Repository;

use App\Entity\DetallePedido;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<DetallePedido>
 *
 * @method DetallePedido|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetallePedido|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetallePedido[]    findAll()
 * @method DetallePedido[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetallePedidoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DetallePedido::class);
    }

    /**
     * @return DetallePedido[] Returns an array of DetalleEntrega objects
     */
    public function devolverPedidosNoEntregadosPorPersona(int $idPersona): array
    {
        return $this->createQueryBuilder('dp')
            ->join('dp.pedido', 'ped')
            ->join('ped.persona', 'per')
            ->where('ped.seEntrego = :seEntrego')
            ->andWhere('per.id = :personaId')
            ->setParameter('seEntrego', false)
            ->setParameter('personaId', $idPersona)
            ->orderBy('dp.id', 'ASC') // Reemplaza 'algunCampo' con el campo que desees ordenar
            ->getQuery()
            ->getResult();
    }

//    /**
//     * @return DetallePedido[] Returns an array of DetallePedido objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('d.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?DetallePedido
//    {
//        return $this->createQueryBuilder('d')
//            ->andWhere('d.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
