<?php

namespace App\Repository;

use App\Entity\Persona;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Persona>
 *
 * @method Persona|null find($id, $lockMode = null, $lockVersion = null)
 * @method Persona|null findOneBy(array $criteria, array $orderBy = null)
 * @method Persona[]    findAll()
 * @method Persona[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PersonaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Persona::class);
    }

    /**
     * @return Persona[] Returns an array of Persona objects
     */
    public function devolverPersonasDeLosPedidosActivos(): array
    {
        return $this->createQueryBuilder('per')
            ->innerJoin('per.pedidos', 'ped')
            ->andWhere('ped.seEntrego = :entrega')
            ->setParameter('entrega', false)
            ->orderBy('per.razonSocial', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return QueryBuilder Returns an array of Persona objects
     */
    public function devolverQueryBuilderPersonasDeLosPedidosActivos(): QueryBuilder
    {
        return $this->createQueryBuilder('per')
            ->innerJoin('per.pedidos', 'ped')
            ->andWhere('ped.seEntrego = :entrega')
            ->setParameter('entrega', true)
            ->orderBy('per.razonSocial', 'ASC');
    }

    /**
     * @return QueryBuilder Returns an array of Persona objects
     */
    public function devolverPersonasCliente(): QueryBuilder
    {
        return $this->createQueryBuilder('per')
            ->where('per.tipo = :tipo1')
            ->orWhere('per.tipo = :tipo2')
            ->setParameter('tipo1', Persona::TIPO_CLIENTE)
            ->setParameter('tipo2', Persona::TIPO_AMBOS);
    }

}
