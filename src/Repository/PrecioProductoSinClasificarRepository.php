<?php

namespace App\Repository;

use App\Entity\PrecioProductoSinClasificar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<PrecioProductoSinClasificar>
 *
 * @method PrecioProductoSinClasificar|null find($id, $lockMode = null, $lockVersion = null)
 * @method PrecioProductoSinClasificar|null findOneBy(array $criteria, array $orderBy = null)
 * @method PrecioProductoSinClasificar[]    findAll()
 * @method PrecioProductoSinClasificar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PrecioProductoSinClasificarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PrecioProductoSinClasificar::class);
    }

//    /**
//     * @return PreciosProductosSinClasificar[] Returns an array of PreciosProductosSinClasificar objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?PreciosProductosSinClasificar
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
