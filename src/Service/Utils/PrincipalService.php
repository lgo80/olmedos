<?php

namespace App\Service\Utils;


use App\Entity\Monto;
use App\Repository\MonedaRepository;
use Symfony\Bundle\SecurityBundle\Security;

class PrincipalService
{

    public function __construct(
        private readonly MonedaRepository $monedaRepository,
        private readonly Security         $security
    )
    {
    }

    public final const SIGNO_PESOS = '$';
    public final const SIGNO_DOLARES = 'U$S';


    public function formatearNumero(
        float   $numero, bool $conParentesis,
        ?string $signo = null): string
    {
        $signo = $signo ?? $this->security->getUser()->getMoneda()->getSigno();
        if ($numero >= 0)
            return $signo . ' '
                . number_format($numero, 2,
                    ',', '.');
        return $conParentesis
            ? '(' . $signo . ' ' . number_format($numero * (-1), 2,
                ',', '.') . ')'
            : $signo . ' ' . number_format($numero, 2,
                ',', '.');
    }

    public function formatearMonto(Monto $monto): string
    {
        $userLogin = $this->security->getUser();
        $valor = $this->devolverImporte(
            $userLogin->getMoneda()->getNombre(),
            $monto->getMoneda()->getNombre(),
            $monto->getImporte(),
            $monto->getTipoCambio());
        return $monto->getBase() == 1
            ? $userLogin->getMoneda()->getSigno() . ' '
            . number_format($valor, 2,
                ',', '.')
            : '(' . $userLogin->getMoneda()->getSigno() . ' '
            . number_format($valor, 2,
                ',', '.') . ')';
    }

    /*public function devolverImporteEnMonedaActual(Monto $monto): float|int
    {
        $userLogin = $this->security->getUser();
        return $this->devolverImporte(
            $userLogin->getMoneda()->getNombre(),
            $monto->getMoneda()->getNombre(),
            $monto->getImporte() * $monto->getBase(),
            $monto->getTipoCambio());
    }*/

    public function formatearMontoDeLosDetalles(
        float $numero, Monto $montoUsado): string
    {
        $userLogin = $this->security->getUser();
        $montoParaDetalle = $this->devolverImporte(
            $userLogin->getMoneda()->getNombre(),
            $montoUsado->getMoneda()->getNombre(),
            $numero, $montoUsado->getTipoCambio());
        return $userLogin->getMoneda()->getSigno() . ' '
            . number_format($montoParaDetalle,
                2, ',', '.');
    }

    private function devolverImporte(
        string    $nombreMonedaActual,
        string    $nombreMonedaDetalle,
        float|int $importe,
        float|int $tipoCambio): float|int
    {
        if ($nombreMonedaActual == 'Pesos')
            return $nombreMonedaDetalle == 'Pesos'
                ? $importe
                : $importe * $tipoCambio;
        else
            return $nombreMonedaDetalle == 'Pesos'
                ? $importe / $tipoCambio
                : $importe;
    }

    public function getTipoCambio(): ?float
    {
        return $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ])->getTipoCambio();
    }

    public function devolverImporteInt(
        Monto $monto): float|int
    {
        $userLogin = $this->security->getUser();
        $montoInt = $monto->getImporte() * $monto->getBase();
        if ($userLogin->getMoneda()->getNombre() == 'Pesos')
            return $monto->getMoneda()->getNombre() == 'Pesos'
                ? $montoInt
                : $montoInt * $monto->getTipoCambio();
        else
            return $monto->getMoneda()->getNombre() == 'Pesos'
                ? $montoInt / $monto->getTipoCambio()
                : $montoInt;
    }
}