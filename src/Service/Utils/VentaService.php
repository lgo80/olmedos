<?php

namespace App\Service\Utils;

use App\Entity\Transaccion;
use App\Entity\Venta;

class VentaService
{

    public function __construct(
        private readonly PrincipalService $principalService

    )
    {
    }

    public function deudaGeneradaPorLaEntrega(
        Venta $ventaNueva): float|int
    {
        return $this->getImporteDeLaEntrega($ventaNueva) - $this->principalService
                ->devolverImporteInt($ventaNueva->getMonto());
    }

    public function getImporteDeLaEntrega(
        Venta $venta): float|int
    {
        $importeEntrega = $this->principalService
            ->devolverImporteInt($venta->getEntrega()->getMonto());
        $importeTransacciones = array_reduce($venta->getTransacciones(),
            fn(mixed $initial, Transaccion $transaccion) => $initial +=
                $this->principalService
                    ->devolverImporteInt($transaccion->getMonto()), $initial = 0);
        return $importeEntrega + $importeTransacciones;
    }

}