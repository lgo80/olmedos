<?php

namespace App\Service\Utils;

use App\Entity\Caja;
use App\Repository\CajaRepository;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\Request;

class CajaService
{

    public function __construct(
        private readonly CajaRepository   $cajaRepository,
        private readonly CalendarService  $calendarService,
        private readonly PrincipalService $principalService
    )
    {
    }

    /**
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function __invoke(
        Request $request
    ): array
    {
        $cajas = $this->cajaRepository->findAll();
        $cajasFiltradas = array_filter($cajas,
            fn(Caja $caja) => $this->estaDentroDelRangoDeFecha($request, $caja));
        $orden = $request->request->get('txtOrdenCaja') ? $request->request->get('txtOrdenCaja') : 'asc';
        if ($orden == 'desc')
            usort($cajasFiltradas, fn($a, $b) => strtotime($b->getFecha()->format('d-m-Y')) - strtotime($a->getFecha()->format('d-m-Y')));
        else
            usort($cajasFiltradas, fn($b, $a) => strtotime($b->getFecha()->format('d-m-Y')) - strtotime($a->getFecha()->format('d-m-Y')));
        $total = $orden == 'asc' ? 0 : $this->generarTotal($cajasFiltradas);
        return [
            [
                'cajas' => $cajasFiltradas,
                'orden' => $orden,
                'fechadesde' => $request->request->get('fechadesde'),
                'fechahasta' => $request->request->get('fechahasta'),
                'total' => $total
            ],
            null
        ];
    }

    /**
     * @throws Exception
     */
    private function estaDentroDelRangoDeFecha(Request $request, Caja $caja): bool
    {
        $fechadesde = $request->request->get('fechadesde');
        $fechahasta = $request->request->get('fechahasta');
        $formato = "Y-m-d\TH:i";
        $fechadesde = DateTime::createFromFormat($formato, $fechadesde);
        $fechahasta = DateTime::createFromFormat($formato, $fechahasta);
        return ($fechadesde && $fechahasta)
            ? $this->calendarService->validarDentroDelRangoEntrante($fechadesde, $fechahasta, $caja->getFecha())
            : $this->calendarService->validarDentroDelMesEnCurso($caja->getFecha());
    }

    /**
     * @param Caja[] $cajasFiltradas
     * @return int|float
     */
    private function generarTotal(array $cajasFiltradas): int|float
    {
        return array_reduce($cajasFiltradas,
            fn(mixed $initial, Caja $caja) => $initial +=
                $this->principalService
                    ->devolverImporteInt($caja->getSaldo()),
            $initial = 0);
    }
}