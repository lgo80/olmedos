<?php

namespace App\Service\Utils;

use App\Entity\ListaDePrecios;
use App\Repository\ListaDePreciosRepository;
use Symfony\Component\HttpFoundation\Request;

class ListaDePreciosService
{
    public function __construct(
        private readonly ListaDePreciosRepository $listaDePreciosRepository
    )
    {
    }

    public function __invoke(
        Request $request
    ): array
    {
        $porcentaje = $request->request->get('txtPorcentaje');
        if ($porcentaje < 0)
            return [null, true];
        $listas = $this->listaDePreciosRepository->findAll();
        foreach ($listas as $lista) {
            $formLista = $request->request->get('chkLista-' . $lista->getId());
            if (!$formLista)
                continue;
            $lista->aumentarListaAsociada($porcentaje);
            $this->listaDePreciosRepository->update($lista);
        }
        return [true, null];
    }

    /**
     * @param ListaDePrecios[] $listaDePreciosDelProducto
     * @param int $idPersona
     * @return ListaDePrecios|null
     */
    public function getListaDePrecioDelaPersona(
        array $listaDePreciosDelProducto,
        int   $idPersona): ?ListaDePrecios
    {
        foreach ($listaDePreciosDelProducto as $listaDePrecios) {
            if ($listaDePrecios->getTipo() != 1)
                return $listaDePrecios;
            if ($listaDePrecios->verificarPersonaEnLaLista($idPersona))
                return $listaDePrecios;
        }
        return null;
    }
}