<?php

namespace App\Service\Utils;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;

/**
 * Created by PhpStorm.
 * User: Isabella
 * Date: 28/04/2017
 * Time: 08:39 PM
 */
class CalendarService
{
    const DIAS = array(
        "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"
    );
    const MESES = array(
        "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto",
        "Septiembre", "Octubre", "Noviembre", "Diciembre"
    );
    private DateTimeZone $dtz;

    function __construct()
    {
        $this->dtz = new DateTimeZone("America/Argentina/Buenos_Aires");
    }

    /**
     * @throws Exception
     */
    public function validarDentroDelMesEnCurso(DateTimeImmutable|DateTime $fechaAControlar): bool
    {
        return $fechaAControlar->format('Y-m') === $this->getDateToday()->format('Y-m');
    }

    public function validarDentroDelRangoEntrante(
        DateTimeImmutable|DateTime $fechadesde,
        DateTimeImmutable|DateTime $fechahasta,
        DateTimeImmutable|DateTime $fechaAControlar
    ): bool
    {
        return $fechaAControlar >= $fechadesde && $fechaAControlar <= $fechahasta;
    }

    /**
     * Esta funcion devuelve la fecha actual de Buenos aires
     * @return DateTime
     * @throws Exception
     */
    public function getDateToday(): DateTime
    {
        return new DateTime("now", $this->dtz);
    }

    /**
     * Esta funcion devuelve una fecha específica
     * @param int $day
     * @param int $month
     * @param int $year
     * @param int|null $hour
     * @param int|null $minute
     * @param int|null $second
     * @return DateTime
     * @throws Exception
     */
    public function getSpecificDate(
        int  $day,
        int  $month,
        int  $year,
        ?int $hour = 0,
        ?int $minute = 0,
        ?int $second = 0
    ): DateTime
    {
        return new DateTime($day . '-' . $month . '-' . $year . 'T' . $hour . ':' . $minute . ':' . $second, $this->dtz);
    }

    /**
     * Esta funcion le suma días a la fecha actual de buenos aires
     * @param int $day
     * @return DateTime
     * @throws Exception
     */
    public function getSumDays(int $day): DateTime
    {
        return $this->getDateToday()->add(new DateInterval('P' . $day . 'D'));
    }

    /**
     * Esta funcion le resta días a la fecha actual de buenos aires
     * @param int $day
     * @return DateTime
     * @throws Exception
     */
    public function getSubtractDays(int $day): DateTime
    {
        return $this->getDateToday()->sub(new DateInterval('P' . $day . 'D'));
    }

    /**
     * Esta funcion le suma meses a la fecha actual de buenos aires
     * @param int $month
     * @return DateTime
     * @throws Exception
     */
    public function getSumMonth(int $month): DateTime
    {
        return $this->getDateToday()->add(new DateInterval('P' . $month . 'M'));
    }

    /**
     * Esta funcion le resta meses a la fecha actual de buenos aires
     * @param int $month
     * @return DateTime
     * @throws Exception
     */
    public function getSubtractMonth(int $month): DateTime
    {
        return $this->getDateToday()->sub(new DateInterval('P' . $month . 'M'));
    }

    /**
     * Esta funcion le suma horas a la fecha actual de buenos aires
     * @param int $hour
     * @return DateTime
     * @throws Exception
     */
    public function getSumHour(int $hour): DateTime
    {
        return $this->getDateToday()->add(new DateInterval('PT' . $hour . 'H'));
    }

    /**
     * Esta funcion le resta horas a la fecha actual de buenos aires
     * @param int $hour
     * @return DateTime
     * @throws Exception
     */
    public function getSubtractHour(int $hour): DateTime
    {
        return $this->getDateToday()->sub(new DateInterval('PT' . $hour . 'H'));
    }


    /**
     * Esta funcion compara que la fecha del primer parametro sea mayor
     * que la del segundo, Si no se ingresa el segundo parametro se
     * compara con la fecha actual
     * @param DateTimeInterface $fechaPosterior Fecha que se quiere comparar
     * @param DateTimeInterface|null $fechaAnterior Fecha de base para comparar si es vacia es la fecha actual
     * @return bool true: Si la primera fecha ingresada es posterior a la segunda / false: La segunda fecha ingresada es anterior a la primera
     * @throws Exception
     */
    public function isTheDateGreaterThan(
        DateTimeInterface  $fechaPosterior,
        ?DateTimeInterface $fechaAnterior = null): bool
    {
        if (!$fechaAnterior)
            $fechaAnterior = $this->getDateToday();
        return $fechaPosterior->format('Y-m-d H:i:s') > $fechaAnterior->format('Y-m-d H:i:s');
    }

    /**
     * Esta funcion devuelve la fecha que no sea nula
     * @param DateTimeInterface $date
     * @return DateTimeInterface
     */
    public function isDateNotNull(DateTimeInterface $date): DateTimeInterface
    {
        return $date;
    }

    /**
     * Esta funcion devuelve la fecha formateada tipo:
     * Lunes, 23 de enero del 2022 a las 13:45hs
     * @param $diaSemana
     * @param $diaNumero
     * @param $mes
     * @param $anno
     * @param $hora
     * @return string
     */
    private function generateDateSpanish(
        $diaSemana,
        $diaNumero,
        $mes,
        $anno,
        $hora
    ): string
    {
        return self::DIAS[$diaSemana - 1] . ", " .
            $diaNumero . " de " . self::MESES[$mes - 1]
            . " del " . $anno . " a las " . $hora . "hs";
    }

    /**
     * Esta funcion devuelve la fecha tipo string formateado tipo:
     * Lunes, 23 de enero del 2022 a las 13:45hs
     * @param DateTimeInterface|null $date
     * @return string|null
     */
    public function formattedDateSpanishOnly(?DateTimeInterface $date): ?string
    {
        return ($date != null)
            ? $this->generateDateSpanish(
                $date->format('N'),
                $date->format('d'),
                $date->format('m'),
                $date->format('Y'),
                $date->format('H:i:s')
            ) : null;
    }

    /**
     * @param DateTime $fechaDate
     * @param string $dia 'hoy': si se quiere validar con la fecha de hoy, 'ayer' si se quiere validar con la fecha de ayer y anteayer
     * @return bool
     * @throws Exception
     */
    public function getDateTodayTotal(DateTime $fechaDate, string $dia): bool
    {
        $dateInit = ($dia == 'hoy')
            ? (clone $fechaDate)->setTime(0, 1)->setTimezone($this->dtz)
            : (clone $fechaDate)->sub(new DateInterval('P2D'))->setTime(0, 0)->setTimezone($this->dtz);
        $dateEnd = ($dia == 'hoy')
            ? (clone $fechaDate)->setTimezone($this->dtz)
            : (clone $fechaDate)->sub(new DateInterval('P1D'))->setTime(23, 59)->setTimezone($this->dtz);
        $fechaToday = $this->getDateToday()->setTimezone($this->dtz);
        return ($this->isTheDateGreaterThan($fechaToday, $dateInit) && $this->isTheDateGreaterThan($dateEnd, $fechaToday));
    }

    /**
     * @param DateTime $fechaObjetivo
     * @return DateInterval|bool
     */
    public function calculateRestantTime(DateTime $fechaObjetivo): DateInterval|bool
    {
        // Crea objetos DateTime para la fecha actual y la fecha objetivo
        $fechaActual = new DateTime();
        // Calcula la diferencia entre las fechas
        return $fechaActual->diff($fechaObjetivo);
    }
}
