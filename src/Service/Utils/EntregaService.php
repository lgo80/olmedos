<?php

namespace App\Service\Utils;

use App\Entity\Caja;
use App\Entity\DetalleEntrega;
use App\Entity\DetallePedido;
use App\Entity\Entrega;
use App\Entity\Moneda;
use App\Entity\Monto;
use App\Entity\Pedido;
use App\Entity\Transaccion;
use App\Entity\User;
use App\Entity\Venta;
use App\Repository\CajaRepository;
use App\Repository\EntregaRepository;
use App\Repository\PedidoRepository;
use App\Repository\TransaccionRepository;
use DateTimeImmutable;
use Symfony\Component\Form\FormInterface;

class EntregaService
{
    public function __construct(
        private readonly PedidoRepository      $pedidoRepository,
        private readonly EntregaRepository     $entregaRepository,
        private readonly CajaRepository        $cajaRepository,
        private readonly PrincipalService      $principalService,
        private readonly TransaccionRepository $transaccionRepository,
        private readonly VentaService          $ventaService
    )
    {
    }

    public function actualizarPedidosEntregados(
        Entrega $entrega): void
    {
        $pedidosNoEntregados = $this->pedidoRepository->findBy([
            'persona' => $entrega->getPersona(),
            'seEntrego' => false
        ]);
        $pedidoNuevo = $this->generarPedidoNuevo($entrega);
        foreach ($pedidosNoEntregados as $pedidoNoEntregado) {
            $this->actualizarPedido($pedidoNoEntregado, $pedidoNuevo, $entrega);
        }
        if ($pedidoNuevo->getDetallePedidos() && count($pedidoNuevo->getDetallePedidos()) > 0) {
            $pedidoNuevo->getMonto()->setImporte($pedidoNuevo->getTotal());
            $this->pedidoRepository->save($pedidoNuevo, true);
        }
    }

    private function actualizarPedido(
        Pedido  $pedidoNoEntregado,
        Pedido  $pedidoNuevo,
        Entrega $entrega): void
    {
        foreach ($pedidoNoEntregado->getDetallePedidos() as $detallePedido) {
            $detalleEntrega = $entrega->devolverDetalleEntrega($detallePedido);
            if (!$detalleEntrega) {
                $pedidoNoEntregado->removeDetallePedido($detallePedido);
                $pedidoNuevo->addDetallePedido($detallePedido);
                continue;
            }
            if ($detalleEntrega->getCantidad() < $detallePedido->getCantidad()) {
                $detallePedidoNuevo = $this->generarDetallePedidoNuevo(
                    $pedidoNuevo, $detallePedido, $detalleEntrega);
                $pedidoNuevo->addDetallePedido($detallePedidoNuevo);
                $detallePedido->setCantidad($detalleEntrega->getCantidad());
            }
        }

        if ($pedidoNoEntregado->getDetallePedidos() && count($pedidoNoEntregado->getDetallePedidos()) > 0) {
            $pedidoNoEntregado->setSeEntrego(true);
            $this->pedidoRepository->save($pedidoNoEntregado, true);
        } else {
            $this->pedidoRepository->save($pedidoNuevo, true);
            $this->pedidoRepository->remove($pedidoNoEntregado, true);
        }
    }

    private function generarPedidoNuevo(Entrega $entrega): Pedido
    {
        $pedidoNuevo = new Pedido();
        $pedidoNuevo->setPersona($entrega->getPersona());
        $pedidoNuevo->setFechaAt(new DateTimeImmutable());
        $pedidoNuevo->setMonto($this->generarMontoNuevo(
            1, $entrega->getMonto()->getMoneda(), 0));
        return $pedidoNuevo;
    }

    /**
     * @param Pedido $pedidoNuevo
     * @param DetallePedido $detallePedido
     * @param DetalleEntrega $detalleEntrega
     * @return DetallePedido
     */
    private function generarDetallePedidoNuevo(
        Pedido         $pedidoNuevo,
        DetallePedido  $detallePedido,
        DetalleEntrega $detalleEntrega): DetallePedido
    {
        $detallePedidoMod = new DetallePedido();
        $detallePedidoMod->setCantidad($detallePedido->getCantidad() - $detalleEntrega->getCantidad());
        $detallePedidoMod->setPrecio($detalleEntrega->getPrecio());
        $detallePedidoMod->setProducto($detalleEntrega->getDetallePedido()->getProducto());
        return $detallePedidoMod;
    }

    /**
     * @param Entrega $entrega
     * @param Transaccion[] $transacciones
     * @param Venta $ventaNueva
     * @param FormInterface $form
     * @return void
     */
    public function guardarCobro(
        Entrega       $entrega,
        array         $transacciones,
        Venta         $ventaNueva,
        FormInterface $form
    ): void
    {
        $monto = $this->generarMontoNuevo(
            1,
            $form->get('moneda')->getNormData(),
            $form->get('importe')->getNormData()
        );
        $ventaNueva->setMonto($monto);
        $entrega->setVenta($ventaNueva);
        $entrega->setSePago(true);
        foreach ($transacciones as $transaccion) {
            $transaccion->setEstaCobrado(true);
            $ventaNueva->addTransaccion($transaccion);
//            $transaccion->setVenta($ventaNueva);
//            $this->transaccionRepository->save($transaccion, true);
        }
        $this->entregaRepository
            ->save($entrega, true);
        $caja = new Caja();
        $caja->setVenta($ventaNueva);
        $this->cajaRepository->save($caja, true);
    }

    public function generarDeudaNueva(
        Venta $ventaNueva,
        User  $user): void
    {
        $deuda = $this->ventaService
            ->deudaGeneradaPorLaEntrega($ventaNueva);
        if ($deuda == 0)
            return;
        $transaccionNueva = new Transaccion();
        $transaccionNueva->setPersona($ventaNueva->getCliente());
        $transaccionNueva->setTipo(Transaccion::TIPO_DEUDA);
        $transaccionNueva->setFechaTransaccion(new DateTimeImmutable());
        $montoNuevo = $this->generarMontoNuevo(
            $deuda > 0 ? 1 : (-1),
            $user->getMoneda(),
            $deuda < 0 ? ($deuda * (-1)) : $deuda
        );
        $transaccionNueva->setMonto($montoNuevo);
        $this->transaccionRepository->save($transaccionNueva, true);
    }


    private function generarMontoNuevo(
        float|int $base,
        Moneda    $moneda,
        float|int $importe): Monto
    {
        $montoNuevo = new Monto();
        $montoNuevo->setBase($base);
        $montoNuevo->setMoneda($moneda);
        $montoNuevo->setImporte($importe);
        $montoNuevo->setTipoCambio(
            $this->principalService->getTipoCambio());
        return $montoNuevo;
    }

}