<?php

namespace App\DataFixtures;

use App\Entity\Ciudad;
use App\Entity\Provincia;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CiudadFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $this->generateCiudad($manager, 'Ciudad autonoma de buenos aires', 1);
        $this->generateCiudad($manager, 'Gran Buenos aires', 1);
        $this->generateCiudad($manager, 'Mendoza', 4);
        $this->generateCiudad($manager, 'Rosario', 3);
        $this->generateCiudad($manager, 'Córdoba', 2);
        $manager->flush();
    }

    private function generateCiudad(
        ObjectManager $manager,
        string        $nombre,
        int           $id_provincia): void
    {
        $moneda = new Ciudad();
        $moneda->setNombre($nombre);
        $provincia = $manager->find(Provincia::class, $id_provincia);
        $moneda->setProvincia($provincia);
        $manager->persist($moneda);
    }
}