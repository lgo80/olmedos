<?php

namespace App\DataFixtures;

use App\Entity\Pais;
use App\Entity\Provincia;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProvinciaFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $this->generateProvincia($manager, 'Buenos Aires', 1);
        $this->generateProvincia($manager, 'Córdoba', 1);
        $this->generateProvincia($manager, 'Santa fe', 1);
        $this->generateProvincia($manager, 'Mendoza', 1);
        $this->generateProvincia($manager, 'Corrientes', 1);
        $this->generateProvincia($manager, 'Catamarca', 1);
        $this->generateProvincia($manager, 'Chaco', 1);
        $this->generateProvincia($manager, 'Chubut', 1);
        $this->generateProvincia($manager, 'Entre Ríos', 1);
        $this->generateProvincia($manager, 'Formosa', 1);
        $this->generateProvincia($manager, 'Jujuy', 1);
        $this->generateProvincia($manager, 'La Pampa', 1);
        $this->generateProvincia($manager, 'La Rioja', 1);
        $this->generateProvincia($manager, 'Misiones', 1);
        $this->generateProvincia($manager, 'Neuquen', 1);
        $this->generateProvincia($manager, 'Río Negro', 1);
        $this->generateProvincia($manager, 'Salta', 1);
        $this->generateProvincia($manager, 'San Juan', 1);
        $this->generateProvincia($manager, 'San Luís', 1);
        $this->generateProvincia($manager, 'Santa Cruz', 1);
        $this->generateProvincia($manager, 'Santiago del Estero', 1);
        $this->generateProvincia($manager, 'Tierra del Fuego', 1);
        $this->generateProvincia($manager, 'Tucumán', 1);
        $manager->flush();
    }

    private function generateProvincia(
        ObjectManager $manager,
        string        $nombre,
        int           $id_pais): void
    {
        $moneda = new Provincia();
        $moneda->setNombre($nombre);
        $pais = $manager->find(Pais::class, $id_pais);
        $moneda->setPais($pais);
        $manager->persist($moneda);
    }

}