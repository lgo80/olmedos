<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class Aaaafixture extends Fixture implements DependentFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // ...
    }

    /**
     * @inheritDoc
     */
    public function getDependencies(): array
    {
        return [
            MonedaFixtures::class,
            MateriaPrimaFixtures::class,
            UserFixtures::class,
            ClasificacionDeLasBobinasFixtures::class,
            ProductoFixtures::class,
            PaisFixtures::class,
            ProvinciaFixtures::class,
            CiudadFixtures::class
        ];
    }

}