<?php

namespace App\DataFixtures;

use App\Entity\ClasificacionDeBobinas;
use App\Entity\Producto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductoFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        /*
         * Las Bobinas de cinta
         */
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 45,
            13, 51.3, 8, Producto::MATERIAL_ALUMINIO, null,
            150, Producto::TIPO_BOBINA_CINTA, 1);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 45,
            14, 51.3, 8, Producto::MATERIAL_COBRE, null,
            200, Producto::TIPO_BOBINA_CINTA, 1);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            20, 51.3, 8, Producto::MATERIAL_COBRE, null,
            200, Producto::TIPO_BOBINA_CINTA, 1);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 1,
            1, 51.3, 1, Producto::MATERIAL_COBRE, null,
            1, Producto::TIPO_BOBINA_CINTA, 1);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 45,
            12, 62.7, 8, Producto::MATERIAL_ALUMINIO, null,
            300, Producto::TIPO_BOBINA_CINTA, 2);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            16, 62.7, 8, Producto::MATERIAL_ALUMINIO, null,
            300, Producto::TIPO_BOBINA_CINTA, 2);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            16, 62.7, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_CINTA, 2);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            20, 62.7, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_CINTA, 2);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 1,
            1, 62.7, 1, Producto::MATERIAL_COBRE, null,
            1, Producto::TIPO_BOBINA_CINTA, 2);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 45,
            12, 76.3, 8, Producto::MATERIAL_ALUMINIO, null,
            400, Producto::TIPO_BOBINA_CINTA, 3);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            16, 76.3, 8, Producto::MATERIAL_ALUMINIO, null,
            400, Producto::TIPO_BOBINA_CINTA, 3);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            16, 76.3, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 3);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            20, 76.3, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 3);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 1,
            1, 76.3, 1, Producto::MATERIAL_ALUMINIO, null,
            1, Producto::TIPO_BOBINA_CINTA, 3);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            20, 99, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 5);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 45,
            12, 99.4, 8, Producto::MATERIAL_ALUMINIO, null,
            600, Producto::TIPO_BOBINA_CINTA, 4);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            16, 99.4, 8, Producto::MATERIAL_ALUMINIO, null,
            600, Producto::TIPO_BOBINA_CINTA, 4);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            16, 99.4, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 4);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            20, 99.4, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 5);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            26, 99.4, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 6);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            11, 101.7, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 4);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            16, 101.7, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 4);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 1,
            1, 101.7, 1, Producto::MATERIAL_COBRE, null,
            1, Producto::TIPO_BOBINA_CINTA, 4);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 70,
            20, 101.7, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 5);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 75,
            20, 100.5, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 5);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            20, 103, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_CINTA, 5);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 1,
            1, 101.7, 1, Producto::MATERIAL_COBRE, null,
            1, Producto::TIPO_BOBINA_CINTA, 5);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 1,
            1, 101.7, 1, Producto::MATERIAL_COBRE, null,
            1, Producto::TIPO_BOBINA_CINTA, 6);
        /*
         * Las Bobinas de Alambre
        */
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 10,
            6, 25.5, 1, Producto::MATERIAL_COBRE, null,
            30, Producto::TIPO_BOBINA_ALAMBRE, 7);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 35,
            11, 32.2, 8, Producto::MATERIAL_COBRE, null,
            80, Producto::TIPO_BOBINA_ALAMBRE, 8);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 40,
            10, 38.6, 8, Producto::MATERIAL_COBRE, null,
            80, Producto::TIPO_BOBINA_ALAMBRE, 9);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 40,
            16, 38.6, 8, Producto::MATERIAL_COBRE, null,
            80, Producto::TIPO_BOBINA_ALAMBRE, 9);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 40,
            10, 39.5, 8, Producto::MATERIAL_COBRE, null,
            80, Producto::TIPO_BOBINA_ALAMBRE, 9);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 40,
            16, 40.6, 8, Producto::MATERIAL_COBRE, null,
            100, Producto::TIPO_BOBINA_ALAMBRE, 10);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 35,
            10, 44.8, 8, Producto::MATERIAL_COBRE, null,
            100, Producto::TIPO_BOBINA_ALAMBRE, 10);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            18, 49, 8, Producto::MATERIAL_COBRE, null,
            100, Producto::TIPO_BOBINA_ALAMBRE, 10);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 50.6, 8, Producto::MATERIAL_COBRE, null,
            200, Producto::TIPO_BOBINA_ALAMBRE, 11);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 60, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 60.6, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 61.2, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 63.2, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 63.5, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 64, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 64.6, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 50,
            14, 65.2, 8, Producto::MATERIAL_COBRE, null,
            400, Producto::TIPO_BOBINA_ALAMBRE, 12);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 60,
            18, 75, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_ALAMBRE, 13);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 60,
            18, 75.5, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_ALAMBRE, 13);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 60,
            18, 76, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_ALAMBRE, 13);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 60,
            18, 76.3, 8, Producto::MATERIAL_COBRE, null,
            600, Producto::TIPO_BOBINA_ALAMBRE, 13);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 60,
            16, 99.8, 8, Producto::MATERIAL_COBRE, null,
            800, Producto::TIPO_BOBINA_ALAMBRE, 14);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 60,
            20, 99.8, 8, Producto::MATERIAL_COBRE, null,
            800, Producto::TIPO_BOBINA_ALAMBRE, 15);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 65,
            26, 99.8, 8, Producto::MATERIAL_COBRE, null,
            800, Producto::TIPO_BOBINA_ALAMBRE, 16);
        $this->generateProducto($manager, null, Producto::TIPO_PRODUCTO_BOBINA, 73,
            33, 99.8, 8, Producto::MATERIAL_COBRE, '4 capas',
            1000, Producto::TIPO_BOBINA_ALAMBRE, 17);
        /*
         * Parlantes nuevos
        */
        $this->generateProducto($manager, '6BE150',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 6, 8, null, null,
            150, null, 0);
        $this->generateProducto($manager, '8BE150',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 8, 8, null, null,
            150, null, 0);
        $this->generateProducto($manager, '10BE150',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 10, 8, null, null,
            150, null, 0);
        $this->generateProducto($manager, '10BF400',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 10, 8, null, null,
            400, null, 0);
        $this->generateProducto($manager, '12BE150',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 12, 8, null, null,
            150, null, 0);
        $this->generateProducto($manager, '12BF400',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 12, 8, null, null,
            400, null, 0);
        $this->generateProducto($manager, '12BF600',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 12, 8, null, null,
            600, null, 0);
        $this->generateProducto($manager, '12BF1000',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 12, 8, null, null,
            1000, null, 0);
        $this->generateProducto($manager, '15BE150',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 15, 8, null, null,
            150, null, 0);
        $this->generateProducto($manager, '15BF400',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 15, 8, null, null,
            400, null, 0);
        $this->generateProducto($manager, '15BF600',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 15, 8, null, null,
            600, null, 0);
        $this->generateProducto($manager, '15BF1000',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 15, 8, null, null,
            1000, null, 0);
        $this->generateProducto($manager, '18BF400',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 18, 8, null, null,
            400, null, 0);
        $this->generateProducto($manager, '18BF600',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 18, 8, null, null,
            600, null, 0);
        $this->generateProducto($manager, '18BF1000',
            Producto::TIPO_PRODUCTO_PARLANTE, null,
            null, 18, 8, null, null,
            1000, null, 0);
        /*
         * Reparaciones
        */
        $this->generateProducto($manager, '6nn<2>', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina menor a 2"',
            null, null, 0);
        $this->generateProducto($manager, '6nn2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 2"',
            null, null, 0);
        $this->generateProducto($manager, '6nn21/2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 21/2"',
            null, null, 0);
        $this->generateProducto($manager, '8nn<2>', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina menor a 2"',
            null, null, 0);
        $this->generateProducto($manager, '8nn2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 2"',
            null, null, 0);
        $this->generateProducto($manager, '8nn21/2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 21/2"',
            null, null, 0);
        $this->generateProducto($manager, '8nn3', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 3"',
            null, null, 0);
        $this->generateProducto($manager, '10nn<2>', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina menor a 2"',
            null, null, 0);
        $this->generateProducto($manager, '10nn2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 2"',
            null, null, 0);
        $this->generateProducto($manager, '10nn21/2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 21/2"',
            null, null, 0);
        $this->generateProducto($manager, '10nn3', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 3"',
            null, null, 0);
        $this->generateProducto($manager, '12nn<2>', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina menor a 2"',
            null, null, 0);
        $this->generateProducto($manager, '12nn2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 2"',
            null, null, 0);
        $this->generateProducto($manager, '12nn21/2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 21/2"',
            null, null, 0);
        $this->generateProducto($manager, '12nn3', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 3"',
            null, null, 0);
        $this->generateProducto($manager, '12nn4', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 4"',
            null, null, 0);
        $this->generateProducto($manager, '15nn<2>', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina menor a 2"',
            null, null, 0);
        $this->generateProducto($manager, '15nn2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 2"',
            null, null, 0);
        $this->generateProducto($manager, '15nn21/2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 21/2"',
            null, null, 0);
        $this->generateProducto($manager, '15nn3', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 3"',
            null, null, 0);
        $this->generateProducto($manager, '15nn4', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 4"',
            null, null, 0);
        $this->generateProducto($manager, '18nn2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 2"',
            null, null, 0);
        $this->generateProducto($manager, '18nn21/2', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 21/2"',
            null, null, 0);
        $this->generateProducto($manager, '18nn3', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 3"',
            null, null, 0);
        $this->generateProducto($manager, '18nn4', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'La bobina de 4"',
            null, null, 0);
        /*
         * Drivers y otros
         */
        $this->generateProducto($manager, 'Repuesto driver chico', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Menor a 50 watts',
            null, null, 0);
        $this->generateProducto($manager, 'Repuesto driver grande', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Mayor a 50 watts',
            null, null, 0);
        $this->generateProducto($manager, 'Reparacion unidades AP', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Cambio Foam <=6', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Cambio Foam 8', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Cambio Foam 10', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Cambio Foam 12', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Cambio Foam 15', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Cambio Copo chico', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Cambio Copo grande', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, 'Cualquier potencia',
            null, null, 0);
        $this->generateProducto($manager, 'Diafragma PM60', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, '60 watts',
            null, null, 0);
        $this->generateProducto($manager, 'Unidad PM60', Producto::TIPO_PRODUCTO_REPARACION, null,
            null, null, null, null, '60 watts',
            null, null, 0);
        $manager->flush();
    }

    private function generateProducto(
        ObjectManager $manager,
        ?string       $detalle,
        int           $tipoProducto,
        ?int          $altura,
        ?int          $bobinado,
        ?float        $diametro,
        ?int          $impedancia,
        ?int          $material,
        ?string       $nota,
        ?int          $potemcia,
        ?int          $tipoBobina,
        int           $clasificacionBobinas_id): void
    {
        $producto = new Producto();
        $producto->setDetalle($detalle);
        $producto->setTipoProducto($tipoProducto);
        $producto->setAltura($altura);
        $producto->setBobinado($bobinado);
        $producto->setDiametro($diametro);
        $producto->setImpedancia($impedancia);
        $producto->setMaterial($material);
        $producto->setNota($nota);
        $producto->setPotencia($potemcia);
        $producto->setTipoBobina($tipoBobina);
        $clasificacionDeLasBobinas = $manager->find(ClasificacionDeBobinas::class, $clasificacionBobinas_id);
        $producto->setClasificacionDeBobinas($clasificacionDeLasBobinas);
        $manager->persist($producto);
    }
}