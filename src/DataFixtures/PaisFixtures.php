<?php

namespace App\DataFixtures;

use App\Entity\Pais;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PaisFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        $this->generatePais($manager, 'Argentina');
        $this->generatePais($manager, 'Uruguay');
        $this->generatePais($manager, 'Brasil');
        $this->generatePais($manager, 'Chile');
        $this->generatePais($manager, 'Paraguay');
        $this->generatePais($manager, 'Perú');
        $this->generatePais($manager, 'Bolivia');
        $manager->flush();
    }

    private function generatePais(
        ObjectManager $manager,
        string        $nombre): void
    {
        $moneda = new Pais();
        $moneda->setNombre($nombre);
        $manager->persist($moneda);
    }
}