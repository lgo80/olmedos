<?php

namespace App\DataFixtures;

use App\Entity\ClasificacionDeBobinas;
use App\Entity\Moneda;
use App\Entity\Producto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MonedaFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        $this->generateProducto($manager, 'Pesos', '$', 1);
        $this->generateProducto($manager, 'Dólares', 'U$S', 955);
        $manager->flush();
    }

    private function generateProducto(
        ObjectManager $manager,
        string        $nombre,
        string        $signo,
        string        $tipoCambio): void
    {
        $moneda = new Moneda();
        $moneda->setNombre($nombre);
        $moneda->setSigno($signo);
        $moneda->setTipoCambio($tipoCambio);
        $manager->persist($moneda);
    }
}