<?php

namespace App\DataFixtures;

use App\Entity\Moneda;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{

    public function __construct(
        private readonly UserPasswordHasherInterface $userPasswordHasher
    )
    {
    }

    public function load(ObjectManager $manager): void
    {
        $this->generateUser($manager, 'UTN_#2032#', User::ROLE_ADMIN, 'lgo80@yahoo.com.ar', 1);
        $this->generateUser($manager, 'racing_05', User::ROLE_USER, 'mdo_1978@yahoo.com.ar', 2);
        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string $pass
     * @param string $role
     * @param string $email
     * @param int $moneda_id
     * @return void
     */
    private function generateUser(
        ObjectManager $manager,
        string        $pass,
        string        $role,
        string        $email,
        int           $moneda_id): void
    {
        $user = new User();
        $password = $this->userPasswordHasher
            ->hashPassword($user, $pass);
        $user->setPassword($password);
        $user->setRoles([$role]);
        $user->setEmail($email);
        $user->setIsVerified(true);
        $moneda = $manager->find(Moneda::class, $moneda_id);
        $user->setMoneda($moneda);
        $manager->persist($user);
    }
}
