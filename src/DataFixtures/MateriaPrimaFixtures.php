<?php

namespace App\DataFixtures;

use App\Entity\MateriaPrima;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class MateriaPrimaFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        $this->generateMateriaPrima($manager, 'Suspenciones');
        $manager->flush();
    }

    private function generateMateriaPrima(
        ObjectManager $manager,
        string        $detalle): void
    {
        $materiaPrima = new MateriaPrima();
        $materiaPrima->setDetalle($detalle);
        $manager->persist($materiaPrima);
    }
}