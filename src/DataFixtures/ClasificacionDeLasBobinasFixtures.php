<?php

namespace App\DataFixtures;

use App\Entity\ClasificacionDeBobinas;
use App\Entity\Producto;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ClasificacionDeLasBobinasFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        $this->generateClasificacion($manager, '2" Cinta'); //1
        $this->generateClasificacion($manager, '21/2" Cinta'); //2
        $this->generateClasificacion($manager, '3" Cinta'); //3
        $this->generateClasificacion($manager, '4a" Cinta'); //4
        $this->generateClasificacion($manager, '4b" Cinta'); //5
        $this->generateClasificacion($manager, '4c" Cinta'); //6
        $this->generateClasificacion($manager, '1" Alambre'); //7
        $this->generateClasificacion($manager, '11/4" Alambre'); //8
        $this->generateClasificacion($manager, '11/2" Alambre'); //9
        $this->generateClasificacion($manager, '13/4" Alambre'); //10
        $this->generateClasificacion($manager, '2" Alambre'); //11
        $this->generateClasificacion($manager, '21/2" Alambre'); //12
        $this->generateClasificacion($manager, '3" Alambre'); //13
        $this->generateClasificacion($manager, '4a" Alambre'); //14
        $this->generateClasificacion($manager, '4b" Alambre'); //15
        $this->generateClasificacion($manager, '4c" Alambre'); //16
        $this->generateClasificacion($manager, '4d" Alambre'); //17
        $manager->flush();
    }

    private function generateClasificacion(
        ObjectManager $manager,
        ?string       $nombre): void
    {
        $clasificacionDeBobinas = new ClasificacionDeBobinas();
        $clasificacionDeBobinas->setNombre($nombre);
        $manager->persist($clasificacionDeBobinas);
    }
}