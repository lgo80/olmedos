<?php

namespace App\DataFixtures;

use App\Entity\MateriaPrima;
use App\Entity\Persona;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PersonaFixtures extends Fixture
{

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager): void
    {
        $this->generatePersona($manager, 'email1@gmail.com','Hugo Ramos', 'Montiel', Persona::TIPO_CLIENTE);
        $this->generatePersona($manager, 'email2@gmail.com','Carlos', 'Cammilleri', Persona::TIPO_AMBOS);
        $this->generatePersona($manager, 'email3@gmail.com','Camufo', 'Camufo', Persona::TIPO_PROVEEDOR);
        $this->generatePersona($manager, 'email4@gmail.com','Leo Tabia', 'Universo Sonido', Persona::TIPO_CLIENTE);
        $manager->flush();
    }

    private function generatePersona(
        ObjectManager $manager,
        string        $email,
        string        $nombre,
        string        $razonSocial,
        int        $tipo): void
    {
        $persona = new Persona();
        $persona->setEmail($email);
        $persona->setNombre($nombre);
        $persona->setRazonSocial($razonSocial);
        $persona->setTipo($tipo);
        $manager->persist($persona);
    }
}