<?php

namespace App\EventSubscriber;

use App\Entity\Compra;
use App\Entity\Entrega;
use App\Entity\ListaDePrecios;
use App\Entity\Pago;
use App\Entity\Pedido;
use App\Entity\Transaccion;
use App\Repository\CajaRepository;
use App\Repository\MonedaRepository;
use DateTimeImmutable;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\AfterEntityUpdatedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EasyAdminSubscriber implements EventSubscriberInterface
{

    /**
     */
    #[Pure] public function __construct(
        private readonly CajaRepository   $cajaRepository,
        private readonly MonedaRepository $monedaRepository
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            BeforeCrudActionEvent::class => ['setEntityModifiedBeforeAction'],
            AfterEntityPersistedEvent::class => ['setEntityModified'],
            BeforeEntityPersistedEvent::class => ['setEntityPersistBefore'],
            BeforeEntityUpdatedEvent::class => ['setEntityUpdateBefore'],
            AfterEntityUpdatedEvent::class => ['setEntityUpdateAfter'],
        ];
    }

    /**
     * @param AfterEntityPersistedEvent $event
     * @return void
     */
    public function setEntityModified(AfterEntityPersistedEvent $event): void
    {
        $entity = $event->getEntityInstance();
        if ($entity instanceof Compra || $entity instanceof Transaccion || $entity instanceof Pago)
            $this->cajaRepository->grabarGasto($entity);
    }

    /**
     * @param AfterEntityUpdatedEvent $event
     * @return void
     */
    public function setEntityUpdateAfter(AfterEntityUpdatedEvent $event): void
    {
        $entity = $event->getEntityInstance();
        $entity->setDateUpdatedAt(new DateTimeImmutable());
    }

    public function setEntityPersistBefore(BeforeEntityPersistedEvent $event): void
    {
        $this->procesarPersistencia($event);
    }

    public function setEntityUpdateBefore(BeforeEntityUpdatedEvent $event): void
    {
        $this->procesarPersistencia($event);
    }

    /**
     * @param BeforeEntityPersistedEvent|BeforeEntityUpdatedEvent $event
     * @return void
     */
    public function procesarPersistencia(BeforeEntityPersistedEvent|BeforeEntityUpdatedEvent $event): void
    {
        $entity = $event->getEntityInstance();
        $entity->setDateUpdatedAt(new DateTimeImmutable());
        if ($entity instanceof Compra)
            $this->agregarCompra($entity);
        else if ($entity instanceof Pago)
            $this->agregarPago($entity);
        else if ($entity instanceof Transaccion)
            $this->agregarTransaccion($entity);
        else if ($entity instanceof Pedido)
            $this->agregarPedido($entity);
        else if ($entity instanceof Entrega)
            $this->agregarentrega($entity, $event);
        else if ($entity instanceof ListaDePrecios)
            $this->agregarListaPrecios($entity, $event);
    }

    /**
     * @param Pago $pago
     * @return void
     */
    private function agregarPago(Pago $pago): void
    {
        $pago->getMonto()->setBase(-1);
        $dolar = $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ]);
        $pago->getMonto()->setTipoCambio($dolar->getTipoCambio());
    }

    private function agregarTransaccion(Transaccion $transaccion): void
    {
        $dolar = $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ]);
        $transaccion->getMonto()->setTipoCambio($dolar->getTipoCambio());
    }

    private function agregarCompra(Compra $compra): void
    {
        $compra->getMonto()->setBase(-1);
        $compra->getMonto()->setImporte($compra->getTotalEnNumero());
        $dolar = $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ]);
        $compra->getMonto()->setTipoCambio($dolar->getTipoCambio());
    }

    private function agregarPedido(Pedido $pedido): void
    {
        $pedido->getMonto()->setBase(1);
        $pedido->getMonto()->setImporte($pedido->getTotal());
        $dolar = $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ]);
        $pedido->getMonto()->setTipoCambio($dolar->getTipoCambio());
    }

    public function setEntityModifiedBeforeAction(BeforeCrudActionEvent $event): void
    {
        $entity = $event->getAdminContext()->getEntity()->getFqcn();
        $page = $event->getAdminContext()->getRequest()->get('crudAction');
        if ($entity != Entrega::class || $page != 'edit')
            return;

    }

    private function agregarentrega(
        Entrega                                             $entrega,
        BeforeEntityPersistedEvent|BeforeEntityUpdatedEvent $event): void
    {
        $entrega->getMonto()->setBase(1);
        $importe = $event instanceof BeforeEntityPersistedEvent
            ? 0 : $entrega->getTotal();
        $entrega->getMonto()->setImporte($importe);
        $dolar = $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ]);
        $entrega->getMonto()->setTipoCambio($dolar->getTipoCambio());
    }

    private function agregarListaPrecios(
        ListaDePrecios                                      $listaDePrecios,
        BeforeEntityPersistedEvent|BeforeEntityUpdatedEvent $event): void
    {
        $dolar = $this->monedaRepository->findOneBy([
            'nombre' => 'Dólares'
        ]);
        $arrayPrecios = ($listaDePrecios->getTipo() == 1)
            ? $listaDePrecios->getPreciosDeLasClasificaciones()
            : $listaDePrecios->getPreciosProductosSinClasificars();
        foreach ($arrayPrecios as $precioDelArray)
            $precioDelArray->getMonto()->setTipoCambio($dolar->getTipoCambio());
    }

}