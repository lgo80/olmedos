<?php

namespace App\Entity;

use App\Repository\ClasificacionDeBobinasRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ClasificacionDeBobinasRepository::class)]
class ClasificacionDeBobinas
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 30)]
    private ?string $nombre = null;

    #[ORM\OneToMany(mappedBy: 'clasificacionDeBobinas', targetEntity: Producto::class, orphanRemoval: true)]
    private Collection $productos;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\OneToMany(mappedBy: 'clasificacionDeBobinas', targetEntity: PrecioDeLaClasificacion::class, orphanRemoval: true)]
    private Collection $precioDeLaClasificacion;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->productos = new ArrayCollection();
        $this->precioDeLaClasificacion = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Producto[]
     */
    public function getProductos(): array
    {
        return $this->productos->toArray();
    }

    public function addProductos(Producto $producto): static
    {
        if (!$this->productos->contains($producto)) {
            $this->productos->add($producto);
            $producto->setClasificacionDeBobinas($this);
        }

        return $this;
    }

    public function removeProductos(Producto $producto): static
    {
        if ($this->productos->removeElement($producto)) {
            // set the owning side to null (unless already changed)
            if ($producto->getClasificacionDeBobinas() === $this) {
                $producto->setClasificacionDeBobinas(null);
            }
        }

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    /**
     * @return PrecioDeLaClasificacion[]
     */
    public function getPreciosDeLasClasificaciones(): array
    {
        return $this->precioDeLaClasificacion->toArray();
    }

    public function addPreciosDeLasClasificacione(PrecioDeLaClasificacion $preciosDeLasClasificacione): static
    {
        if (!$this->precioDeLaClasificacion->contains($preciosDeLasClasificacione)) {
            $this->precioDeLaClasificacion->add($preciosDeLasClasificacione);
            $preciosDeLasClasificacione->setClasificacionDeBobinas($this);
        }

        return $this;
    }

    public function removePreciosDeLasClasificacione(PrecioDeLaClasificacion $preciosDeLasClasificacione): static
    {
        if ($this->precioDeLaClasificacion->removeElement($preciosDeLasClasificacione)) {
            // set the owning side to null (unless already changed)
            if ($preciosDeLasClasificacione->getClasificacionDeBobinas() === $this) {
                $preciosDeLasClasificacione->setClasificacionDeBobinas(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->nombre;
    }

    /**
     * @return Producto[]
     */
    public function obtenerProductosOrdenados(): array
    {
        $productosNoOrdenados = $this->getProductos();
        usort($productosNoOrdenados,
            fn(Producto $productoA, Producto $productoB) => strcmp(
                $productoA->getNombreCompleto(), $productoB->getNombreCompleto()));
        return $productosNoOrdenados;
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
