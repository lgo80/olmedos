<?php

namespace App\Entity;

use App\Repository\PedidoRepository;
use App\Service\Utils\PrincipalService;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PedidoRepository::class)]
class Pedido
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?DateTimeImmutable $fechaAt = null;

    #[ORM\ManyToOne(inversedBy: 'pedidos')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Persona $persona = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\Column(length: 1000, nullable: true)]
    private ?string $nota = null;

    #[ORM\Column]
    private bool $seEntrego = false;

    #[ORM\OneToMany(mappedBy: 'pedido', targetEntity: DetallePedido::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $detallePedidos;

    #[ORM\Column]
    private bool $estaActivo = true;

    #[ORM\ManyToOne(inversedBy: 'pedido')]
    private ?Entrega $entrega = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Monto $monto = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->detallePedidos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaAt(): ?DateTimeImmutable
    {
        return $this->fechaAt;
    }

    public function setFechaAt(?DateTimeImmutable $fechaAt): static
    {
        $this->fechaAt = $fechaAt;

        return $this;
    }

    public function getPersona(): ?Persona
    {
        return $this->persona;
    }

    public function setPersona(?Persona $persona): static
    {
        $this->persona = $persona;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): static
    {
        $this->nota = $nota;

        return $this;
    }

    public function isSeEntrego(): ?bool
    {
        return $this->seEntrego;
    }

    public function setSeEntrego(bool $seEntrego): static
    {
        $this->seEntrego = $seEntrego;

        return $this;
    }

    /**
     * @return DetallePedido[]
     */
    public function getDetallePedidos(): array
    {
        return $this->detallePedidos->toArray();
    }

    public function addDetallePedido(DetallePedido $detallePedido): static
    {
        if (!$this->detallePedidos->contains($detallePedido)) {
            $this->detallePedidos->add($detallePedido);
            $detallePedido->setPedido($this);
        }

        return $this;
    }

    public function removeDetallePedido(DetallePedido $detallePedido): static
    {
        if ($this->detallePedidos->removeElement($detallePedido)) {
            // set the owning side to null (unless already changed)
            if ($detallePedido->getPedido() === $this) {
                $detallePedido->setPedido(null);
            }
        }

        return $this;
    }

    public function isEstaActivo(): bool
    {
        return $this->estaActivo;
    }

    public function setEstaActivo(bool $estaActivo): static
    {
        $this->estaActivo = $estaActivo;

        return $this;
    }

    public function getEntrega(): ?Entrega
    {
        return $this->entrega;
    }

    public function setEntrega(?Entrega $entrega): static
    {
        $this->entrega = $entrega;

        return $this;
    }

    public function getMonto(): ?Monto
    {
        return $this->monto;
    }

    public function setMonto(Monto $monto): static
    {
        $this->monto = $monto;

        return $this;
    }

    public function __toString(): string
    {
        return $this->persona;
    }

    public function getTotalFormateado(): string
    {
        return '';
    }


    public function getTotal(): float|int
    {
        return array_reduce($this->getDetallePedidos(),
            fn(mixed $initial, DetallePedido $detallePedido) => $initial +=
                $detallePedido->getTotal(), $initial = 0);
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
