<?php

namespace App\Entity;

use App\Repository\DetallePedidoRepository;
use App\Service\Utils\PrincipalService;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DetallePedidoRepository::class)]
class DetallePedido
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'detallePedidos')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Producto $producto = null;

    #[ORM\Column]
    private ?int $cantidad = null;

    #[ORM\Column]
    private ?float $precio = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'detallePedidos')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Pedido $pedido;

    #[ORM\OneToMany(mappedBy: 'detallePedido', targetEntity: DetalleEntrega::class, orphanRemoval: true)]
    private Collection $detalleEntregas;

    #[ORM\Column(length: 1000, nullable: true)]
    private ?string $nota = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->detalleEntregas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): static
    {
        $this->producto = $producto;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): static
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): static
    {
        $this->precio = $precio;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getPedido(): ?Pedido
    {
        return $this->pedido;
    }

    public function setPedido(?Pedido $pedido): static
    {
        $this->pedido = $pedido;

        return $this;
    }

    /**
     * @return DetalleEntrega[]
     */
    public function getDetalleEntregas(): array
    {
        return $this->detalleEntregas->toArray();
    }

    public function addDetalleEntrega(DetalleEntrega $detalleEntrega): static
    {
        if (!$this->detalleEntregas->contains($detalleEntrega)) {
            $this->detalleEntregas->add($detalleEntrega);
            $detalleEntrega->setDetallePedido($this);
        }

        return $this;
    }

    public function removeDetalleEntrega(DetalleEntrega $detalleEntrega): static
    {
        if ($this->detalleEntregas->removeElement($detalleEntrega)) {
            // set the owning side to null (unless already changed)
            if ($detalleEntrega->getDetallePedido() === $this) {
                $detalleEntrega->setDetallePedido(null);
            }
        }

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): static
    {
        $this->nota = $nota;

        return $this;
    }

    public function getTotal(): float|int
    {
        return $this->cantidad * $this->precio;
    }

    public function __toString(): string
    {
        return $this->cantidad . ' - '
            . $this->producto
            . ' a ' . $this->getSigno() . ' ' . $this->precio . ' c/u - Total: '
            . $this->getSigno() . ' ' . $this->getTotal();
    }

    public function getSigno(): string
    {
        return $this->pedido->getMonto()->getMoneda()->getSigno();
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

}
