<?php

namespace App\Entity;

use App\Repository\ProductoRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductoRepository::class)]
class Producto
{

    public final const TIPO_PRODUCTO_BOBINA = 1;
    public final const TIPO_PRODUCTO_PARLANTE = 2;
    public final const TIPO_PRODUCTO_REPARACION = 3;
    public final const TIPO_BOBINA_CINTA = 1;
    public final const TIPO_BOBINA_ALAMBRE = 2;
    public final const MATERIAL_COBRE = 1;
    public final const MATERIAL_ALUMINIO = 2;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $detalle = null;

    #[ORM\Column(nullable: true)]
    private ?int $impedancia = null;

    #[ORM\Column(nullable: true)]
    private ?int $potencia = null;

    #[ORM\Column]
    private bool $isActivo = true;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nota = null;

    #[ORM\Column(nullable: true)]
    private ?float $diametro = null;

    #[ORM\Column(nullable: true)]
    private ?int $bobinado = null;

    #[ORM\Column(nullable: true)]
    private ?int $altura = null;

    #[ORM\Column(nullable: true)]
    private ?int $tipoBobina = null;

    #[ORM\Column(nullable: true)]
    private ?int $material = null;

    #[ORM\Column]
    private ?int $tipoProducto = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\ManyToOne(inversedBy: 'productos')]
    #[ORM\JoinColumn(nullable: true)]
    private ?ClasificacionDeBobinas $clasificacionDeBobinas = null;

    #[ORM\OneToMany(mappedBy: 'producto', targetEntity: DetallePedido::class)]
    #[ORM\OrderBy(['detalle' => 'ASC', 'diametro' => 'ASC', 'bobinado' => 'ASC', 'impedancia' => 'ASC', 'material' => 'ASC', 'potencia' => 'ASC'])]
    private Collection $detallePedidos;

    #[ORM\OneToMany(mappedBy: 'producto', targetEntity: PrecioProductoSinClasificar::class, orphanRemoval: true)]
    private Collection $preciosProductosSinClasificars;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->detallePedidos = new ArrayCollection();
        $this->preciosProductosSinClasificars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDetalle(): ?string
    {
        return $this->detalle;
    }

    public function setDetalle(?string $detalle): static
    {
        $this->detalle = $detalle;

        return $this;
    }

    public function getImpedancia(): ?int
    {
        return $this->impedancia;
    }

    public function setImpedancia(?int $impedancia): static
    {
        $this->impedancia = $impedancia;

        return $this;
    }

    public function getPotencia(): ?int
    {
        return $this->potencia;
    }

    public function setPotencia(?int $potencia): static
    {
        $this->potencia = $potencia;

        return $this;
    }

    public function isIsActivo(): ?bool
    {
        return $this->isActivo;
    }

    public function setIsActivo(bool $isActivo): static
    {
        $this->isActivo = $isActivo;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): static
    {
        $this->nota = $nota;

        return $this;
    }

    public function getDiametro(): ?float
    {
        return $this->diametro;
    }

    public function setDiametro(?float $diametro): static
    {
        $this->diametro = $diametro;

        return $this;
    }

    public function getBobinado(): ?int
    {
        return $this->bobinado;
    }

    public function setBobinado(?int $bobinado): static
    {
        $this->bobinado = $bobinado;

        return $this;
    }

    public function getAltura(): ?int
    {
        return $this->altura;
    }

    public function setAltura(?int $altura): static
    {
        $this->altura = $altura;

        return $this;
    }

    public function getTipoBobina(): ?int
    {
        return $this->tipoBobina;
    }

    public function getTipoBobinaStr(): ?string
    {
        return ($this->tipoBobina == 1) ? 'Cinta' : 'Alambre';
    }

    public function getTipoBobinaParaNombre(): ?string
    {
        return ($this->tipoBobina == 1) ? 'C' : 'A';
    }

    public function setTipoBobina(?int $tipoBobina): static
    {
        $this->tipoBobina = $tipoBobina;

        return $this;
    }

    public function getMaterial(): ?int
    {
        return $this->material;
    }

    public function getMaterialStr(): ?string
    {
        return $this->material == 1 ? 'Cobre' : 'Aluminio';
    }

    public function getMaterialParaNombre(): ?string
    {
        return $this->material == 1 ? 'C' : 'A';
    }

    public function setMaterial(?int $material): static
    {
        $this->material = $material;

        return $this;
    }

    public function getTipoProducto(): ?int
    {
        return $this->tipoProducto;
    }

    public function setTipoProducto(int $tipoProducto): static
    {
        $this->tipoProducto = $tipoProducto;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getNombreCompleto(): string
    {
        return match ($this->tipoProducto) {
            1 => $this->diametro . '-' . $this->bobinado . '-' . $this->altura
                . '-' . $this->impedancia . '-' . $this->getTipoBobinaParaNombre() .
                $this->getMaterialParaNombre(),
            2 => $this->detalle . ' - Parlante nuevo',
            3 => $this->detalle . ' - Reparación',
            default => '',
        };
    }

    public function getClasificacionDeBobinas(): ?ClasificacionDeBobinas
    {
        return $this->clasificacionDeBobinas;
    }

    public function setClasificacionDeBobinas(?ClasificacionDeBobinas $clasificacionDeBobinas): static
    {
        $this->clasificacionDeBobinas = $clasificacionDeBobinas;

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNombreCompleto();
    }

    /**
     * @return DetallePedido[]
     */
    public function getDetallePedidos(): array
    {
        return $this->detallePedidos->toArray();
    }

    public function addDetallePedido(DetallePedido $detallePedido): static
    {
        if (!$this->detallePedidos->contains($detallePedido)) {
            $this->detallePedidos->add($detallePedido);
            $detallePedido->setProducto($this);
        }

        return $this;
    }

    public function removeDetallePedido(DetallePedido $detallePedido): static
    {
        if ($this->detallePedidos->removeElement($detallePedido)) {
            // set the owning side to null (unless already changed)
            if ($detallePedido->getProducto() === $this) {
                $detallePedido->setProducto(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PrecioProductoSinClasificar>
     */
    public function getPreciosProductosSinClasificars(): Collection
    {
        return $this->preciosProductosSinClasificars;
    }

    public function addPreciosProductosSinClasificar(PrecioProductoSinClasificar $preciosProductosSinClasificar): static
    {
        if (!$this->preciosProductosSinClasificars->contains($preciosProductosSinClasificar)) {
            $this->preciosProductosSinClasificars->add($preciosProductosSinClasificar);
            $preciosProductosSinClasificar->setProducto($this);
        }

        return $this;
    }

    public function removePreciosProductosSinClasificar(PrecioProductoSinClasificar $preciosProductosSinClasificar): static
    {
        if ($this->preciosProductosSinClasificars->removeElement($preciosProductosSinClasificar)) {
            // set the owning side to null (unless already changed)
            if ($preciosProductosSinClasificar->getProducto() === $this) {
                $preciosProductosSinClasificar->setProducto(null);
            }
        }

        return $this;
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
