<?php

namespace App\Entity;

use App\Repository\PaisRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PaisRepository::class)]
class Pais
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $nombre = null;

    #[ORM\OneToMany(mappedBy: 'pais', targetEntity: Provincia::class, orphanRemoval: true)]
    private Collection $provincias;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->provincias = new ArrayCollection();
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection<int, Provincia>
     */
    public function getProvincias(): Collection
    {
        return $this->provincias;
    }

    public function addProvincia(Provincia $provincia): static
    {
        if (!$this->provincias->contains($provincia)) {
            $this->provincias->add($provincia);
            $provincia->setPais($this);
        }

        return $this;
    }

    public function removeProvincia(Provincia $provincia): static
    {
        if ($this->provincias->removeElement($provincia)) {
            // set the owning side to null (unless already changed)
            if ($provincia->getPais() === $this) {
                $provincia->setPais(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->nombre;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
