<?php

namespace App\Entity;

use App\Repository\VentaRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VentaRepository::class)]
class Venta
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private DateTime $fechaVenta;

    #[ORM\OneToMany(mappedBy: 'venta', targetEntity: Entrega::class)]
    private Collection $entregas;

    #[ORM\OneToMany(mappedBy: 'venta', targetEntity: Transaccion::class, cascade: ['persist'])]
    private Collection $transacciones;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\OneToMany(mappedBy: 'venta', targetEntity: Caja::class)]
    private Collection $cajas;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private Monto $monto;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->entregas = new ArrayCollection();
        $this->transacciones = new ArrayCollection();
        $this->cajas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaVenta(): DateTime
    {
        return $this->fechaVenta;
    }

    public function setFechaVenta(DateTime $fechaVenta): static
    {
        $this->fechaVenta = $fechaVenta;

        return $this;
    }

    /**
     * @return Entrega[]
     */
    public function getEntregas(): array
    {
        return $this->entregas->toArray();
    }

    public function addEntrega(Entrega $entrega): static
    {
        if (!$this->entregas->contains($entrega)) {
            $this->entregas->add($entrega);
            $entrega->setVenta($this);
        }

        return $this;
    }

    public function removeEntrega(Entrega $entrega): static
    {
        if ($this->entregas->removeElement($entrega)) {
            // set the owning side to null (unless already changed)
            if ($entrega->getVenta() === $this) {
                $entrega->setVenta(null);
            }
        }

        return $this;
    }

    /**
     * @return Transaccion[]
     */
    public function getTransacciones(): array
    {
        return $this->transacciones->toArray();
    }

    public function addTransaccion(Transaccion $transaccion): static
    {
        if (!$this->transacciones->contains($transaccion)) {
            $this->transacciones->add($transaccion);
            $transaccion->setVenta($this);
        }

        return $this;
    }

    public function removeTransaccion(Transaccion $transaccion): static
    {
        if ($this->transacciones->removeElement($transaccion)) {
            // set the owning side to null (unless already changed)
            if ($transaccion->getVenta() === $this) {
                $transaccion->setVenta(null);
            }
        }

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    /**
     * @return Caja[]
     */
    public function getCajas(): array
    {
        return $this->cajas->toArray();
    }

    public function addCaja(Caja $caja): static
    {
        if (!$this->cajas->contains($caja)) {
            $this->cajas->add($caja);
            $caja->setVenta($this);
        }

        return $this;
    }

    public function removeCaja(Caja $caja): static
    {
        if ($this->cajas->removeElement($caja)) {
            // set the owning side to null (unless already changed)
            if ($caja->getVenta() === $this) {
                $caja->setVenta(null);
            }
        }

        return $this;
    }

    /**
     * @return Entrega
     */
    public function getEntrega(): Entrega
    {
        $entregas = $this->getEntregas();
        return $entregas[array_key_first($entregas)];
    }

    public function getImportePagado(): string
    {
        return '';
    }

    public function getImporteEntrega(): string
    {
        return '';
    }

    public function getMonto(): ?Monto
    {
        return $this->monto;
    }

    public function setMonto(Monto $monto): static
    {
        $this->monto = $monto;

        return $this;
    }

    public function getCliente(): ?Persona
    {
        return $this->getEntrega()->getPersona();
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
