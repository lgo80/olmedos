<?php

namespace App\Entity;

use App\Repository\CajaRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CajaRepository::class)]
class Caja
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'cajas')]
    private ?Compra $compra = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\ManyToOne(inversedBy: 'cajas')]
    private ?Pago $pago = null;

    #[ORM\ManyToOne(inversedBy: 'cajas')]
    private ?Transaccion $transaccion = null;

    #[ORM\ManyToOne(inversedBy: 'cajas')]
    private ?Venta $venta = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompra(): ?Compra
    {
        return $this->compra;
    }

    public function setCompra(?Compra $compra): static
    {
        $this->compra = $compra;

        return $this;
    }

    public function getDateCreatedAt(): ?DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getPago(): ?Pago
    {
        return $this->pago;
    }

    public function setPago(?Pago $pago): static
    {
        $this->pago = $pago;

        return $this;
    }

    public function getTransaccion(): ?Transaccion
    {
        return $this->transaccion;
    }

    public function setTransaccion(?Transaccion $transaccion): static
    {
        $this->transaccion = $transaccion;

        return $this;
    }

    public function getVenta(): ?Venta
    {
        return $this->venta;
    }

    public function setVenta(?Venta $venta): static
    {
        $this->venta = $venta;

        return $this;
    }

    public function getFecha(): DateTimeImmutable|DateTime|string|null
    {
        if ($this->compra)
            return $this->compra->getFecha();
        if ($this->pago)
            return $this->pago->getDateCreatedAt();
        if ($this->transaccion)
            return $this->transaccion->getDateCreatedAt();
        return $this->venta ? $this->venta->getFechaVenta() : 'No hay fecha';
    }

    public function getPersona(): Persona|string|null
    {
        if ($this->compra)
            return 'Compra a ' . $this->compra->getProveedor();
        if ($this->pago)
            return $this->pago->getSujetoStr();
        if ($this->transaccion)
            return $this->transaccion->getPersona();
        return $this->venta ? $this->venta->getEntrega()->getPersona() : 'No hay sujeto';
    }

    public function getEntrada(): Monto|null|string
    {
        if ($this->compra || $this->pago ||
            ($this->transaccion && $this->transaccion->getTipo() != Transaccion::TIPO_ANTICIPO))
            return '------';
        if ($this->transaccion) {
            $this->transaccion->getMonto()->setBase(1);
            return $this->transaccion->getMonto();
        }
        return $this->venta->getMonto();
    }

    public function getSalida(): Monto|null|string
    {
        if ($this->compra)
            return $this->compra->getMonto();
        return $this->pago ? $this->pago->getMonto() : '------';
    }

    public function getSaldo(): Monto|null //int $saldo, string $orden
    {
        if ($this->compra)
            return $this->compra->getMonto();
        if ($this->pago)
            return $this->pago->getMonto();
        if ($this->transaccion) {
            $this->transaccion->getMonto()->setBase(1);
            return $this->transaccion->getMonto();
        }
        return $this->venta?->getMonto();
    }

    public function getTipo(): string
    {
        if ($this->compra)
            return 'Compra';
        if ($this->pago)
            return 'Gasto';
        if ($this->transaccion)
            return $this->transaccion->getTipoStr();
        return ($this->venta) ? 'Venta' : 'Algo fallo';
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

}
