<?php

namespace App\Entity;

use App\Repository\DireccionRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DireccionRepository::class)]
class Direccion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $calle = null;

    #[ORM\Column(nullable: true)]
    private ?int $numero = null;

    #[ORM\Column(nullable: true)]
    private ?int $departamento = null;

    #[ORM\Column(nullable: true)]
    private ?int $piso = null;

    #[ORM\Column(nullable: true)]
    private ?int $codigoPostal = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'direcciones')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Ciudad $ciudad = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    #[ORM\ManyToOne(inversedBy: 'direcciones')]
    #[ORM\JoinColumn(nullable: false)]
    private Persona $persona;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCalle(): ?string
    {
        return $this->calle;
    }

    public function setCalle(string $calle): static
    {
        $this->calle = $calle;

        return $this;
    }

    public function getNumero(): ?int
    {
        return $this->numero;
    }

    public function setNumero(int $numero): static
    {
        $this->numero = $numero;

        return $this;
    }

    public function getDepartamento(): ?int
    {
        return $this->departamento;
    }

    public function setDepartamento(?int $departamento): static
    {
        $this->departamento = $departamento;

        return $this;
    }

    public function getPiso(): ?int
    {
        return $this->piso;
    }

    public function setPiso(?int $piso): static
    {
        $this->piso = $piso;

        return $this;
    }

    public function getCodigoPostal(): ?int
    {
        return $this->codigoPostal;
    }

    public function setCodigoPostal(?int $codigoPostal): static
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    public function getCiudad(): ?Ciudad
    {
        return $this->ciudad;
    }

    public function setCiudad(?Ciudad $ciudad): static
    {
        $this->ciudad = $ciudad;

        return $this;
    }

    public function __toString(): string
    {
        $retornar = $this->calle . ' ' . $this->numero;
        if ($this->piso)
            $retornar .= ' ' . $this->piso;
        return ($this->departamento)
            ? $retornar . ' ' . $this->departamento
            : $retornar;
    }

    public function getNombreCompleto(): string
    {
        return $this->__toString() . ' - ' . $this->ciudad->getNombreCompleto();
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getDateUpdatedAt(): DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

    public function getPersona(): Persona
    {
        return $this->persona;
    }

    public function setPersona(Persona $persona): static
    {
        $this->persona = $persona;

        return $this;
    }
}

