<?php

namespace App\Entity;

use App\Repository\PersonaRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PersonaRepository::class)]
class Persona
{
    public final const TIPO_CLIENTE = 1;
    public final const TIPO_PROVEEDOR = 2;
    public final const TIPO_AMBOS = 3;
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private string $razonSocial;

    #[ORM\Column(length: 100)]
    private ?string $nombre = null;

    #[ORM\Column(nullable: true)]
    private ?int $cuit = null;

    #[ORM\Column(nullable: true)]
    private ?string $telefono = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $email = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $observacion = null;

    #[ORM\Column]
    private bool $isActivo = true;

    #[ORM\Column]
    private int $tipo = self::TIPO_CLIENTE;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\OneToMany(mappedBy: 'proveedor', targetEntity: Compra::class, orphanRemoval: true)]
    private Collection $compras;

    #[ORM\OneToMany(mappedBy: 'persona', targetEntity: ListaPreciosPersona::class, orphanRemoval: true)]
    private Collection $listaPreciosPersonas;

    #[ORM\OneToMany(mappedBy: 'persona', targetEntity: Pedido::class, orphanRemoval: true)]
    private Collection $pedidos;

    #[ORM\OneToMany(mappedBy: 'persona', targetEntity: Transaccion::class, orphanRemoval: true)]
    private Collection $transaccions;

    #[ORM\OneToMany(mappedBy: 'persona', targetEntity: Entrega::class, orphanRemoval: true)]
    private Collection $entregas;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    #[ORM\OneToMany(mappedBy: 'persona', targetEntity: Direccion::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $direcciones;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->compras = new ArrayCollection();
        $this->listaPreciosPersonas = new ArrayCollection();
        $this->pedidos = new ArrayCollection();
        $this->transaccions = new ArrayCollection();
        $this->entregas = new ArrayCollection();
        $this->direcciones = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRazonSocial(): ?string
    {
        return $this->razonSocial;
    }

    public function setRazonSocial(string $razonSocial): static
    {
        $this->razonSocial = $razonSocial;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getCuit(): ?int
    {
        return $this->cuit;
    }

    public function setCuit(?int $cuit): static
    {
        $this->cuit = $cuit;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(?string $telefono): static
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): static
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function isIsActivo(): ?bool
    {
        return $this->isActivo;
    }

    public function setIsActivo(bool $isActivo): static
    {
        $this->isActivo = $isActivo;

        return $this;
    }

    public function getTipo(): ?int
    {
        return $this->tipo;
    }

    public function setTipo(int $tipo): static
    {
        $this->tipo = $tipo;

        return $this;
    }


    public function getDireccionCompleta(): string
    {
        return ($this->getDirecciones() && count($this->getDirecciones()) > 0)
            ? $this->getDirecciones()[0]->getNombreCompleto()
            : 'No hay dirección cargada';
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    /**
     * @return Compra[]
     */
    public function getCompras(): array
    {
        return $this->compras->toArray();
    }

    public function addCompra(Compra $compra): static
    {
        if (!$this->compras->contains($compra)) {
            $this->compras->add($compra);
            $compra->setProveedor($this);
        }

        return $this;
    }

    public function removeCompra(Compra $compra): static
    {
        if ($this->compras->removeElement($compra)) {
            // set the owning side to null (unless already changed)
            if ($compra->getProveedor() === $this) {
                $compra->setProveedor(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        if ($this->getNombre() && $this->getRazonSocial())
            return $this->getRazonSocial() . ' (' . $this->getNombre() . ')';
        if ($this->getRazonSocial())
            return $this->getRazonSocial();
        return $this->getNombre();
    }

    /**
     * @return ListaPreciosPersona[]
     */
    public function getListaPreciosPersonas(): array
    {
        return $this->listaPreciosPersonas->toArray();
    }

    public function addListaPreciosPersona(ListaPreciosPersona $listaPreciosPersona): static
    {
        if (!$this->listaPreciosPersonas->contains($listaPreciosPersona)) {
            $this->listaPreciosPersonas->add($listaPreciosPersona);
            $listaPreciosPersona->setPersona($this);
        }

        return $this;
    }

    public function removeListaPreciosPersona(ListaPreciosPersona $listaPreciosPersona): static
    {
        if ($this->listaPreciosPersonas->removeElement($listaPreciosPersona)) {
            // set the owning side to null (unless already changed)
            if ($listaPreciosPersona->getPersona() === $this) {
                $listaPreciosPersona->setPersona(null);
            }
        }

        return $this;
    }

    /**
     * @return Pedido[]
     */
    public function getPedidos(): array
    {
        return $this->pedidos->toArray();
    }

    public function addPedido(Pedido $pedido): static
    {
        if (!$this->pedidos->contains($pedido)) {
            $this->pedidos->add($pedido);
            $pedido->setPersona($this);
        }

        return $this;
    }

    public function removePedido(Pedido $pedido): static
    {
        if ($this->pedidos->removeElement($pedido)) {
            // set the owning side to null (unless already changed)
            if ($pedido->getPersona() === $this) {
                $pedido->setPersona(null);
            }
        }

        return $this;
    }

    /**
     * @return Transaccion[]
     */
    public function getTransaccions(): array
    {
        return $this->transaccions->toArray();
    }

    public function addTransaccion(Transaccion $transaccion): static
    {
        if (!$this->transaccions->contains($transaccion)) {
            $this->transaccions->add($transaccion);
            $transaccion->setPersona($this);
        }

        return $this;
    }

    public function removeTransaccion(Transaccion $transaccion): static
    {
        if ($this->transaccions->removeElement($transaccion)) {
            // set the owning side to null (unless already changed)
            if ($transaccion->getPersona() === $this) {
                $transaccion->setPersona(null);
            }
        }

        return $this;
    }

    /**
     * @return Entrega[]
     */
    public function getEntregas(): array
    {
        return $this->entregas->toArray();
    }

    public function addEntrega(Entrega $entrega): static
    {
        if (!$this->entregas->contains($entrega)) {
            $this->entregas->add($entrega);
            $entrega->setPersona($this);
        }

        return $this;
    }

    public function removeEntrega(Entrega $entrega): static
    {
        if ($this->entregas->removeElement($entrega)) {
            // set the owning side to null (unless already changed)
            if ($entrega->getPersona() === $this) {
                $entrega->setPersona(null);
            }
        }

        return $this;
    }

    public function getDateUpdatedAt(): DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

    /**
     * @return Direccion[]
     */
    public function getDirecciones(): array
    {
        return $this->direcciones->toArray();
    }

    public function addDireccione(Direccion $direccione): static
    {
        if (!$this->direcciones->contains($direccione)) {
            $this->direcciones->add($direccione);
            $direccione->setPersona($this);
        }

        return $this;
    }

    public function removeDireccione(Direccion $direccione): static
    {
        if ($this->direcciones->removeElement($direccione)) {
            // set the owning side to null (unless already changed)
            if ($direccione->getPersona() === $this) {
                $direccione->setPersona(null);
            }
        }

        return $this;
    }

}
