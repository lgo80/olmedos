<?php

namespace App\Entity;

use App\Repository\DetalleCompraRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DetalleCompraRepository::class)]
class DetalleCompra
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'detalleCompras')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MateriaPrima $materiaPrima = null;

    #[ORM\ManyToOne(inversedBy: 'detalleCompras')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Compra $compra = null;

    #[ORM\Column]
    private ?int $cantidad = null;

    #[ORM\Column]
    private ?float $precio = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMateriaPrima(): ?MateriaPrima
    {
        return $this->materiaPrima;
    }

    public function setMateriaPrima(?MateriaPrima $materiaPrima): static
    {
        $this->materiaPrima = $materiaPrima;

        return $this;
    }

    public function getCompra(): ?Compra
    {
        return $this->compra;
    }

    public function setCompra(?Compra $compra): static
    {
        $this->compra = $compra;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): static
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): static
    {
        $this->precio = $precio;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function __toString(): string
    {
        return $this->cantidad . ' '
            . $this->materiaPrima->getDetalle()
            . ' a ' . $this->getSigno() . $this->precio . ' c/u - Total: '
            . $this->getSigno() . $this->getTotal();
    }

    public function getTotal(): float|int
    {
        return $this->cantidad * $this->precio;
    }

    public function getSigno(): string
    {
        return $this->compra->getMonto()->getMoneda()->getSigno();
    }

    /*public function getTotalFormateado(): string
    {
        return PrincipalService::formatearNumero($this->getTotal());
    }

    public function getPrecioFormateado(): string
    {
        return PrincipalService::formatearNumero($this->precio);
    }*/

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
