<?php

namespace App\Entity;

use App\Repository\PagoRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PagoRepository::class)]
class Pago
{
    public final const TIPO_SUELDO = 1;
    public final const TIPO_IMPUESTO = 2;
    public final const TIPO_OTROS_GASTOS = 3;
    public final const SUJETO_LEO = 1;
    public final const SUJETO_MARIANO = 2;
    public final const SUJETO_CASA = 3;
    public final const SUJETO_TALLER = 4;
    public final const ARRAY_SUJETO = ['Leo', 'Mariano', 'Casa', 'Taller'];
    public final const ARRAY_TIPO = ['Sueldo', 'Impuesto', 'Otros gastos'];
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private int $tipo;

    #[ORM\Column]
    private int $sujeto;

    #[ORM\Column(length: 1000, nullable: true)]
    private ?string $nota = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\OneToMany(mappedBy: 'pago', targetEntity: Caja::class)]
    private Collection $cajas;

    #[ORM\Column]
    private ?DateTimeImmutable $fechaPago = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Monto $monto = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTime $periodo = null;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->cajas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTipo(): ?int
    {
        return $this->tipo;
    }

    public function setTipo(int $tipo): static
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getSujeto(): ?int
    {
        return $this->sujeto;
    }

    public function setSujeto(int $sujeto): static
    {
        $this->sujeto = $sujeto;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): static
    {
        $this->nota = $nota;

        return $this;
    }


    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    /**
     * @return Caja[]
     */
    public function getCajas(): array
    {
        return $this->cajas->toArray();
    }

    public function addCaja(Caja $caja): static
    {
        if (!$this->cajas->contains($caja)) {
            $this->cajas->add($caja);
            $caja->setPago($this);
        }

        return $this;
    }

    public function removeCaja(Caja $caja): static
    {
        if ($this->cajas->removeElement($caja)) {
            // set the owning side to null (unless already changed)
            if ($caja->getPago() === $this) {
                $caja->setPago(null);
            }
        }

        return $this;
    }

    public function getSujetoStr(): string
    {
        return self::ARRAY_TIPO[$this->tipo - 1] . ' ' . self::ARRAY_SUJETO[$this->sujeto - 1];
    }

    public function getFechaPago(): ?DateTimeImmutable
    {
        return $this->fechaPago;
    }

    public function setFechaPago(DateTimeImmutable $fechaPago): static
    {
        $this->fechaPago = $fechaPago;

        return $this;
    }

    public function getMonto(): ?Monto
    {
        return $this->monto;
    }

    public function setMonto(Monto $monto): static
    {
        $this->monto = $monto;

        return $this;
    }

    public function __toString(): string
    {
        return 'Pago ' . $this->getSujetoStr();
    }

    public function getMontoFormateado(): string
    {
        return '';
    }

    public function getMonedaQueSePago(): string
    {
        return $this->monto->getMoneda()->getNombre();
    }

    public function getImporteQueSePago(): string
    {
        return $this->monto->getImporte();
    }

    public function getTipoCambioQueSePago(): string
    {
        return $this->monto->getTipoCambio();
    }

    public function getDateUpdatedAt(): DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

    public function getPeriodo(): ?DateTime
    {
        return $this->periodo;
    }

    public function setPeriodo(?DateTime $periodo): static
    {
        $this->periodo = $periodo;

        return $this;
    }

    public function getPeriodoStr(): string
    {
        return $this->periodo
            ? $this->periodo->format("m/y")
            : '---';
    }

    public function estaDentroDelPeriodo(string $periodo): bool
    {
        $fechaHoraObjeto = DateTime::createFromFormat('Y-m-d\TH:i', $periodo);
        return $this->periodo->format("m/y") === $fechaHoraObjeto->format("m/y");
    }
}
