<?php

namespace App\Entity;

use App\Repository\PrecioProductoSinClasificarRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PrecioProductoSinClasificarRepository::class)]
class PrecioProductoSinClasificar
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'preciosProductosSinClasificars')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ListaDePrecios $listaDePrecios = null;

    #[ORM\ManyToOne(inversedBy: 'preciosProductosSinClasificars')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Producto $producto = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Monto $monto = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListaDePrecios(): ?ListaDePrecios
    {
        return $this->listaDePrecios;
    }

    public function setListaDePrecios(?ListaDePrecios $listaDePrecios): static
    {
        $this->listaDePrecios = $listaDePrecios;

        return $this;
    }

    public function getProducto(): ?Producto
    {
        return $this->producto;
    }

    public function setProducto(?Producto $producto): static
    {
        $this->producto = $producto;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getMonto(): ?Monto
    {
        return $this->monto;
    }

    public function setMonto(Monto $monto): static
    {
        $this->monto = $monto;

        return $this;
    }

    public function __toString(): string
    {
        return 'El producto ' . $this->getProducto()->getDetalle()
            . ' de la lista ' . $this->getListaDePrecios()->getNombre()
            . ' sale ' . $this->monto->getMoneda()->getSigno()
            . ' ' . $this->monto->getImporte();
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
