<?php

namespace App\Entity;

use App\Repository\MonedaRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MonedaRepository::class)]
class Moneda
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 20)]
    private ?string $nombre = null;

    #[ORM\Column]
    private ?float $tipoCambio = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\Column(length: 15)]
    private ?string $signo = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipoCambio(): ?float
    {
        return $this->tipoCambio;
    }

    public function setTipoCambio(float $tipoCambio): static
    {
        $this->tipoCambio = $tipoCambio;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getSigno(): ?string
    {
        return $this->signo;
    }

    public function setSigno(string $signo): static
    {
        $this->signo = $signo;

        return $this;
    }

    public function getTipoCambioStr(): ?string
    {
        return $this->tipoCambio;
    }

    public function __toString(): string
    {
        return $this->nombre;
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
