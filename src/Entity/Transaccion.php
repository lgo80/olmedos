<?php

namespace App\Entity;

use App\Repository\TransaccionRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TransaccionRepository::class)]
class Transaccion
{
    public final const TIPO_ARRAY = ['Deuda', 'Anticipo', 'Descuento'];
    public final const TIPO_DEUDA = 1;
    public final const TIPO_ANTICIPO = 2;
    public final const TIPO_DESCUENTO = 3;
    /*    public final const BASE_SUMA = 'A favor de nosotros';
        public final const BASE_RESTA = 'A favor del cliente';*/
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'transaccions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Persona $persona = null;

    #[ORM\Column]
    private ?int $tipo = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\Column]
    private bool $estaCobrado = false;

    #[ORM\OneToMany(mappedBy: 'transaccion', targetEntity: Caja::class)]
    private Collection $cajas;

    #[ORM\ManyToOne(inversedBy: 'transacciones')]
    private ?Venta $venta = null;

    #[ORM\Column]
    private ?DateTimeImmutable $fechaTransaccion = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Monto $monto = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->cajas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPersona(): ?Persona
    {
        return $this->persona;
    }

    public function setPersona(?Persona $persona): static
    {
        $this->persona = $persona;

        return $this;
    }

    public function getTipo(): int
    {
        return $this->tipo;
    }

    public function setTipo(int $tipo): static
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function isEstaCobrado(): ?bool
    {
        return $this->estaCobrado;
    }

    public function setEstaCobrado(bool $estaCobrado): static
    {
        $this->estaCobrado = $estaCobrado;

        return $this;
    }


    /**
     * @return Caja[]
     */
    public function getCajas(): array
    {
        return $this->cajas->toArray();
    }

    public function addCaja(Caja $caja): static
    {
        if (!$this->cajas->contains($caja)) {
            $this->cajas->add($caja);
            $caja->setTransaccion($this);
        }

        return $this;
    }

    public function removeCaja(Caja $caja): static
    {
        if ($this->cajas->removeElement($caja)) {
            // set the owning side to null (unless already changed)
            if ($caja->getTransaccion() === $this) {
                $caja->setTransaccion(null);
            }
        }

        return $this;
    }

    public function getTipoStr(): string
    {
        return self::TIPO_ARRAY[$this->tipo - 1];
    }

    public function getVenta(): ?Venta
    {
        return $this->venta;
    }

    public function setVenta(?Venta $venta): static
    {
        $this->venta = $venta;

        return $this;
    }

    public function getFechaTransaccion(): ?DateTimeImmutable
    {
        return $this->fechaTransaccion;
    }

    public function setFechaTransaccion(DateTimeImmutable $fechaTransaccion): static
    {
        $this->fechaTransaccion = $fechaTransaccion;

        return $this;
    }

    public function getMonto(): ?Monto
    {
        return $this->monto;
    }

    public function setMonto(Monto $monto): static
    {
        $this->monto = $monto;

        return $this;
    }


    public function getMontoEnStr(): string
    {
        return '';
    }

    public function getMonedaQueSePago(): string
    {
        return $this->monto->getMoneda()->getNombre();
    }

    public function getImporteQueSePago(): string
    {
        return $this->monto->getImporte();
    }

    public function getTipoCambioQueSePago(): string
    {
        return $this->monto->getTipoCambio();
    }

    public function __toString(): string
    {
        return 'Transacción ' . $this->id . ' - ' . $this->persona->getRazonSocial();
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

}
