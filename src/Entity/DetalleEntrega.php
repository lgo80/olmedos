<?php

namespace App\Entity;

use App\Repository\DetalleEntregaRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DetalleEntregaRepository::class)]
class DetalleEntrega
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'detalleEntregas')]
    #[ORM\JoinColumn(nullable: false)]
    private ?DetallePedido $detallePedido = null;

    #[ORM\Column]
    private ?int $cantidad = null;

    #[ORM\Column]
    private ?float $precio = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\ManyToOne(inversedBy: 'detalleEntregas')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Entrega $entrega = null;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDetallePedido(): ?DetallePedido
    {
        return $this->detallePedido;
    }

    public function setDetallePedido(?DetallePedido $detallePedido): static
    {
        $this->detallePedido = $detallePedido;

        return $this;
    }

    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    public function setCantidad(int $cantidad): static
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->precio;
    }

    public function setPrecio(float $precio): static
    {
        $this->precio = $precio;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getEntrega(): ?Entrega
    {
        return $this->entrega;
    }

    public function setEntrega(?Entrega $entrega): static
    {
        $this->entrega = $entrega;

        return $this;
    }

    public function __toString(): string
    {
        return $this->cantidad . ' - '
            . $this->detallePedido->getProducto()
            . ' a ' . $this->getSigno() . ' ' . $this->precio . ' c/u - Total: '
            . $this->getSigno() . ' ' . $this->getTotal();
    }

    public function getSigno(): string
    {
        return $this->entrega->getMonto()->getMoneda()->getSigno();
    }

    public function getTotal(): float|int
    {
        return $this->cantidad * $this->precio;
    }

    /*public function getPrecioFormateado(): string
    {
        return PrincipalService::formatearNumero($this->precio);
    }

    public function getTotalFormateado(): string
    {
        return PrincipalService::formatearNumero($this->getTotal());
    }*/

    public function getDateUpdatedAt(): DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

}
