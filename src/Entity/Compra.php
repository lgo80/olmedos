<?php

namespace App\Entity;

use App\Repository\CompraRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CompraRepository::class)]
class Compra
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?DateTimeImmutable $fecha = null;

    #[ORM\ManyToOne(inversedBy: 'compras')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Persona $proveedor = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $nota = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\OneToMany(mappedBy: 'compra', targetEntity: DetalleCompra::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $detalleCompras;

    #[ORM\OneToMany(mappedBy: 'compra', targetEntity: Caja::class)]
    private Collection $cajas;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private Monto $monto;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->detalleCompras = new ArrayCollection();
        $this->cajas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?DateTimeImmutable
    {
        return $this->fecha;
    }

    public function setFecha(?DateTimeImmutable $fecha): static
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getProveedor(): ?Persona
    {
        return $this->proveedor;
    }

    public function setProveedor(?Persona $proveedor): static
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): static
    {
        $this->nota = $nota;

        return $this;
    }

    public function getDateCreatedAt(): ?DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    /**
     * @return DetalleCompra[]
     */
    public function getDetalleCompras(): array
    {
        return $this->detalleCompras->toArray();
    }

    public function addDetalleCompra(DetalleCompra $detalleCompra): static
    {
        if (!$this->detalleCompras->contains($detalleCompra)) {
            $this->detalleCompras->add($detalleCompra);
            $detalleCompra->setCompra($this);
        }

        return $this;
    }

    public function removeDetalleCompra(DetalleCompra $detalleCompra): static
    {
        if ($this->detalleCompras->removeElement($detalleCompra)) {
            // set the owning side to null (unless already changed)
            if ($detalleCompra->getCompra() === $this) {
                $detalleCompra->setCompra(null);
            }
        }

        return $this;
    }

    /**
     * @return Caja[]
     */
    public function getCajas(): array
    {
        return $this->cajas->toArray();
    }

    public function addCaja(Caja $caja): static
    {
        if (!$this->cajas->contains($caja)) {
            $this->cajas->add($caja);
            $caja->setCompra($this);
        }

        return $this;
    }

    public function removeCaja(Caja $caja): static
    {
        if ($this->cajas->removeElement($caja)) {
            // set the owning side to null (unless already changed)
            if ($caja->getCompra() === $this) {
                $caja->setCompra(null);
            }
        }

        return $this;
    }


    public function getTotalEnNumero(): int
    {
        return array_reduce($this->getDetalleCompras(),
            fn(mixed $initial, DetalleCompra $detalleCompra) => $initial +=
                $detalleCompra->getTotal(), $initial = 0);
    }

    public function getMonto(): ?Monto
    {
        return $this->monto;
    }

    public function setMonto(Monto $monto): static
    {
        $this->monto = $monto;

        return $this;
    }

    public function getMontoEnStr(): string
    {
        return '';
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

}
