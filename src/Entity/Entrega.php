<?php

namespace App\Entity;

use App\Repository\EntregaRepository;
use DateTime;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EntregaRepository::class)]
class Entrega
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?DateTime $fechaEntrega = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\Column(length: 1000, nullable: true)]
    private ?string $nota = null;

    #[ORM\Column]
    private bool $seEntrego = false;

    #[ORM\Column]
    private bool $sePago = false;

    #[ORM\Column]
    private bool $activo = true;

    #[ORM\ManyToOne(inversedBy: 'entregas')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Persona $persona = null;

    #[ORM\OneToMany(mappedBy: 'entrega', targetEntity: DetalleEntrega::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $detalleEntregas;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'entregas',)]
    private ?Venta $venta = null;

    #[ORM\OneToOne(cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private ?Monto $monto;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->detalleEntregas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFechaEntrega(): ?DateTime
    {
        return $this->fechaEntrega;
    }

    public function setFechaEntrega(?DateTime $fechaEntrega): static
    {
        $this->fechaEntrega = $fechaEntrega;

        return $this;
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    public function getNota(): ?string
    {
        return $this->nota;
    }

    public function setNota(?string $nota): static
    {
        $this->nota = $nota;

        return $this;
    }

    public function isSeEntrego(): ?bool
    {
        return $this->seEntrego;
    }

    public function setSeEntrego(bool $seEntrego): static
    {
        $this->seEntrego = $seEntrego;

        return $this;
    }

    public function isSePago(): ?bool
    {
        return $this->sePago;
    }

    public function setSePago(bool $sePago): static
    {
        $this->sePago = $sePago;

        return $this;
    }

    public function isActivo(): ?bool
    {
        return $this->activo;
    }

    public function setActivo(bool $activo): static
    {
        $this->activo = $activo;

        return $this;
    }

    public function getPersona(): ?Persona
    {
        return $this->persona;
    }

    public function setPersona(?Persona $persona): static
    {
        $this->persona = $persona;

        return $this;
    }

    /**
     * @return DetalleEntrega[]
     */
    public function getDetalleEntregas(): array
    {
        return $this->detalleEntregas->toArray();
    }

    public function addDetalleEntrega(DetalleEntrega $detalleEntrega): static
    {
        if (!$this->detalleEntregas->contains($detalleEntrega)) {
            $this->detalleEntregas->add($detalleEntrega);
            $detalleEntrega->setEntrega($this);
        }

        return $this;
    }

    public function removeDetalleEntrega(DetalleEntrega $detalleEntrega): static
    {
        if ($this->detalleEntregas->removeElement($detalleEntrega)) {
            // set the owning side to null (unless already changed)
            if ($detalleEntrega->getEntrega() === $this) {
                $detalleEntrega->setEntrega(null);
            }
        }

        return $this;
    }

    public function getTotal(): float|int
    {
        return array_reduce($this->getDetalleEntregas(),
            fn(mixed $initial, DetalleEntrega $detalleEntrega) => $initial +=
                $detalleEntrega->getTotal(), $initial = 0);
    }

    public function getTotalFormateado(): string
    {
        return '';
    }

    public function devolverDetalleEntrega(DetallePedido $detallePedido): ?DetalleEntrega
    {
        $detalles = array_filter($this->getDetalleEntregas(),
            fn(DetalleEntrega $detalleEntrega) => $detalleEntrega->
                getDetallePedido() === $detallePedido);
        return $detalles && count($detalles) > 0 ? $detalles[array_key_first($detalles)] : null;
    }

    public function getVenta(): ?Venta
    {
        return $this->venta;
    }

    public function setVenta(?Venta $venta): static
    {
        $this->venta = $venta;

        return $this;
    }

    public function getMonto(): ?Monto
    {
        return $this->monto;
    }

    public function setMonto(Monto $monto): static
    {
        $this->monto = $monto;

        return $this;
    }

    public function getDateUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(\DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }

}
