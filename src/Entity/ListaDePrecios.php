<?php

namespace App\Entity;

use App\Repository\ListaDePreciosRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ListaDePreciosRepository::class)]
class ListaDePrecios
{

    public final const TIPO_ARRAY = ['Bobinas', 'Parlantes', 'Reparaciones'];
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 30)]
    private ?string $nombre = null;

    #[ORM\Column]
    private ?int $tipo = null;

    #[ORM\Column]
    private DateTimeImmutable $dateCreatedAt;

    #[ORM\OneToMany(mappedBy: 'listaDePrecios', targetEntity: PrecioDeLaClasificacion::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $preciosDeLasClasificaciones;

    #[ORM\OneToMany(mappedBy: 'listaDePrecios', targetEntity: ListaPreciosPersona::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $listaPreciosPersonas;

    #[ORM\OneToMany(mappedBy: 'listaDePrecios', targetEntity: PrecioProductoSinClasificar::class, cascade: ['persist'], orphanRemoval: true)]
    private Collection $preciosProductosSinClasificars;

    #[ORM\Column]
    private DateTimeImmutable $dateUpdatedAt;

    public function __construct()
    {
        $this->dateCreatedAt = new DateTimeImmutable();
        $this->dateUpdatedAt = new DateTimeImmutable();
        $this->preciosDeLasClasificaciones = new ArrayCollection();
        $this->listaPreciosPersonas = new ArrayCollection();
        $this->preciosProductosSinClasificars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipo(): int
    {
        return $this->tipo;
    }

    public function setTipo(int $tipo): static
    {
        $this->tipo = $tipo;

        return $this;
    }

    public function getTipoStr(): string
    {
        return self::TIPO_ARRAY[$this->tipo - 1];
    }

    public function getDateCreatedAt(): DateTimeImmutable
    {
        return $this->dateCreatedAt;
    }

    public function setDateCreatedAt(DateTimeImmutable $dateCreatedAt): static
    {
        $this->dateCreatedAt = $dateCreatedAt;

        return $this;
    }

    /**
     * @return PrecioDeLaClasificacion[]
     */
    public function getPreciosDeLasClasificaciones(): array
    {
        return $this->preciosDeLasClasificaciones->toArray();
    }

    public function addPreciosDeLasClasificacione(PrecioDeLaClasificacion $preciosDeLasClasificacione): static
    {
        if (!$this->preciosDeLasClasificaciones->contains($preciosDeLasClasificacione)) {
            $this->preciosDeLasClasificaciones->add($preciosDeLasClasificacione);
            $preciosDeLasClasificacione->setListaDePrecios($this);
        }
        return $this;
    }

    public function removePreciosDeLasClasificacione(PrecioDeLaClasificacion $preciosDeLasClasificacione): static
    {
        if ($this->preciosDeLasClasificaciones->removeElement($preciosDeLasClasificacione)) {
            // set the owning side to null (unless already changed)
            if ($preciosDeLasClasificacione->getListaDePrecios() === $this) {
                $preciosDeLasClasificacione->setListaDePrecios(null);
            }
        }

        return $this;
    }

    /**
     * @return ListaPreciosPersona[]
     */
    public function getListaPreciosPersonas(): array
    {
        return $this->listaPreciosPersonas->toArray();
    }

    public function addListaPreciosPersona(ListaPreciosPersona $listaPreciosPersona): static
    {
        if (!$this->listaPreciosPersonas->contains($listaPreciosPersona)) {
            $this->listaPreciosPersonas->add($listaPreciosPersona);
            $listaPreciosPersona->setListaDePrecios($this);
        }

        return $this;
    }

    public function removeListaPreciosPersona(ListaPreciosPersona $listaPreciosPersona): static
    {
        if ($this->listaPreciosPersonas->removeElement($listaPreciosPersona)) {
            // set the owning side to null (unless already changed)
            if ($listaPreciosPersona->getListaDePrecios() === $this) {
                $listaPreciosPersona->setListaDePrecios(null);
            }
        }

        return $this;
    }

    public function verificarPersonaEnLaLista(int $idPersona): bool
    {
        $array = array_filter($this->getListaPreciosPersonas(),
            fn(ListaPreciosPersona $listaPrecioPersona) => $listaPrecioPersona->getPersona()->getId() == $idPersona);
        return count($array) > 0;
    }

    public function getPrecioProducto(Producto $producto): ?Monto
    {
        if ($this->tipo == 1)
            $array = array_filter(
                $this->getPreciosDeLasClasificaciones(),
                fn(PrecioDeLaClasificacion $precioDeLaClasificacion) => $precioDeLaClasificacion
                        ->getClasificacionDeBobinas()
                    === $producto->getClasificacionDeBobinas());
        else
            $array = array_filter(
                $this->getPreciosProductosSinClasificars(),
                fn(PrecioProductoSinClasificar $precioProductoSinClasificar) => $precioProductoSinClasificar
                        ->getProducto() === $producto);
        return $array && count($array) > 0 ? $array[array_key_first($array)]->getMonto() : null;
    }

    /**
     * @return PrecioProductoSinClasificar[]
     */
    public function getPreciosProductosSinClasificars(): array
    {
        return $this->preciosProductosSinClasificars->toArray();
    }

    public function addPreciosProductosSinClasificar(PrecioProductoSinClasificar $preciosProductosSinClasificar): static
    {
        if (!$this->preciosProductosSinClasificars->contains($preciosProductosSinClasificar)) {
            $this->preciosProductosSinClasificars->add($preciosProductosSinClasificar);
            $preciosProductosSinClasificar->setListaDePrecios($this);
        }

        return $this;
    }

    public function removePreciosProductosSinClasificar(PrecioProductoSinClasificar $preciosProductosSinClasificar): static
    {
        if ($this->preciosProductosSinClasificars->removeElement($preciosProductosSinClasificar)) {
            // set the owning side to null (unless already changed)
            if ($preciosProductosSinClasificar->getListaDePrecios() === $this) {
                $preciosProductosSinClasificar->setListaDePrecios(null);
            }
        }

        return $this;
    }


    /**
     * @return PrecioDeLaClasificacion[]
     */
    public function obtenerPreciosDeLasClasificacionesOrdenadas(): array
    {
        $clasificacionDeLasBobinasNoOrdenadas = $this->getPreciosDeLasClasificaciones();
        usort($clasificacionDeLasBobinasNoOrdenadas,
            fn(PrecioDeLaClasificacion $clasificacionDeLasBobinasA,
               PrecioDeLaClasificacion $clasificacionDeLasBobinasB) => $clasificacionDeLasBobinasA->getMonto()->getImporte()
                > $clasificacionDeLasBobinasB->getMonto()->getImporte());
        return $clasificacionDeLasBobinasNoOrdenadas;
    }

    /**
     * @return PrecioDeLaClasificacion[]
     */
    public function obtenerPreciosDeLosProductosSinClasificarOrdenadas(): array
    {
        $productosSinClasificarNoOrdenado = $this->getPreciosProductosSinClasificars();
        usort($productosSinClasificarNoOrdenado,
            fn(PrecioProductoSinClasificar $productosSinClasificarA,
               PrecioProductoSinClasificar $productosSinClasificarB) => strcmp(
                    $productosSinClasificarA->getProducto()->getNombreCompleto(),
                    $productosSinClasificarB->getProducto()->getNombreCompleto())
                && $productosSinClasificarA->getMonto()->getImporte()
                > $productosSinClasificarB->getMonto()->getImporte());
        return $productosSinClasificarNoOrdenado;
    }

    public function aumentarListaAsociada(int $porcentaje): void
    {
        if (count($this->preciosDeLasClasificaciones) > 0)
            $this->aumetarPrecioClasificaciones($porcentaje);
        else if (count($this->preciosProductosSinClasificars) > 0)
            $this->aumetarPrecioSinClasificar($porcentaje);
    }

    private function aumetarPrecioClasificaciones(int $porcentaje): void
    {
        foreach ($this->getPreciosDeLasClasificaciones() as $precioDeLasClasificacion)
            $precioDeLasClasificacion->getMonto()->setImporte(ceil(
                $precioDeLasClasificacion->getMonto()->getImporte() * (($porcentaje / 100) + 1)));
    }

    private function aumetarPrecioSinClasificar(int $porcentaje): void
    {
        foreach ($this->getPreciosProductosSinClasificars() as $precioProductoSinClasificar)
            $precioProductoSinClasificar->getMonto()->setImporte(ceil(
                $precioProductoSinClasificar->getMonto()->getImporte() * (($porcentaje / 100) + 1)));
    }

    public function getDateUpdatedAt(): ?DateTimeImmutable
    {
        return $this->dateUpdatedAt;
    }

    public function setDateUpdatedAt(DateTimeImmutable $dateUpdatedAt): static
    {
        $this->dateUpdatedAt = $dateUpdatedAt;

        return $this;
    }
}
